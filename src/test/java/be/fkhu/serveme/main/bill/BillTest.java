package be.fkhu.serveme.main.bill;

import be.fkhu.serveme.account.user.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BillTest {
    Bill billTest = new Bill();
    User userTest = new User();
    {
        billTest.setTotalPrice( 45.5 );
        billTest.setUser( userTest );
    }

    @Test
    void getTotalPrice() {
        assertEquals( billTest.getTotalPrice(), 45.5 );
    }

//    @Test
//    void getDate() {
//    }

    @Test
    void getUser() {
        assertEquals( billTest.getUser(), userTest );
    }

}