package be.fkhu.serveme.main.order;

import be.fkhu.serveme.main.order_menuItem.OrderMenuItem;
import be.fkhu.serveme.main.tables.Table;
import org.junit.Test;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.assertEquals;

class OrderTest {

    Order orderTest = new Order();
    Set<OrderMenuItem>  menuItemTest = new Set<OrderMenuItem>() {
        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        /**
         * Returns an iterator over the elements in this set.  The elements are
         * returned in no particular order (unless this set is an instance of some
         * class that provides a guarantee).
         *
         * @return an iterator over the elements in this set
         */
        @Override
        public Iterator<OrderMenuItem> iterator() {
            return null;
        }


        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return null;
        }

        /**
         * Adds the specified element to this set if it is not already present
         * (optional operation).  More formally, adds the specified element
         * {@code e} to this set if the set contains no element {@code e2}
         * such that
         * {@code Objects.equals(e, e2)}.
         * If this set already contains the element, the call leaves the set
         * unchanged and returns {@code false}.  In combination with the
         * restriction on constructors, this ensures that sets never contain
         * duplicate elements.
         *
         * <p>The stipulation above does not imply that sets must accept all
         * elements; sets may refuse to add any particular element, including
         * {@code null}, and throw an exception, as described in the
         * specification for {@link Collection#add Collection.add}.
         * Individual set implementations should clearly document any
         * restrictions on the elements that they may contain.
         *
         * @param orderMenuItem element to be added to this set
         * @return {@code true} if this set did not already contain the specified
         * element
         * @throws UnsupportedOperationException if the {@code add} operation
         *                                       is not supported by this set
         * @throws ClassCastException            if the class of the specified element
         *                                       prevents it from being added to this set
         * @throws NullPointerException          if the specified element is null and this
         *                                       set does not permit null elements
         * @throws IllegalArgumentException      if some property of the specified element
         *                                       prevents it from being added to this set
         */
        @Override
        public boolean add(OrderMenuItem orderMenuItem) {
            return false;
        }


        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return false;
        }

        /**
         * Adds all of the elements in the specified collection to this set if
         * they're not already present (optional operation).  If the specified
         * collection is also a set, the {@code addAll} operation effectively
         * modifies this set so that its value is the <i>union</i> of the two
         * sets.  The behavior of this operation is undefined if the specified
         * collection is modified while the operation is in progress.
         *
         * @param c collection containing elements to be added to this set
         * @return {@code true} if this set changed as a result of the call
         * @throws UnsupportedOperationException if the {@code addAll} operation
         *                                       is not supported by this set
         * @throws ClassCastException            if the class of an element of the
         *                                       specified collection prevents it from being added to this set
         * @throws NullPointerException          if the specified collection contains one
         *                                       or more null elements and this set does not permit null
         *                                       elements, or if the specified collection is null
         * @throws IllegalArgumentException      if some property of an element of the
         *                                       specified collection prevents it from being added to this set
         */
        @Override
        public boolean addAll(Collection<? extends OrderMenuItem> c) {
            return false;
        }


        @Override
        public boolean retainAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            return false;
        }

        @Override
        public void clear() {

        }
    };
    Set<Table> tableTest = new Set<Table>() {
        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @Override
        public Iterator<Table> iterator() {
            return null;
        }

        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return null;
        }

        @Override
        public boolean add(Table table) {
            return false;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean addAll(Collection<? extends Table> c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            return false;
        }

        @Override
        public void clear() {

        }
    };

    { orderTest.setId( 2L );
    orderTest.setOrderMenuItems(menuItemTest);
   // orderTest.setTables( tableTest );
    }

//        orderTest.setTime(  );




    @Test
    void getIdTest() {
        assertEquals(java.util.Optional.ofNullable( orderTest.getId() ), java.util.Optional.of( 2L ));
    }

//    @Test
//    void getTimeTest() {
//    }
//
    @Test
    void getMenuItemsTest() {
        assertEquals(orderTest.getOrderMenuItems(), menuItemTest);
    }

//    @Test
//    void getTablesTest() {
//        assertEquals(orderTest.getTables(), tableTest);
//    }



}