package be.fkhu.serveme.main.address;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AddressTest {

    Address testAddress = new Address();

    @Test
    void getAddress_ID() {

    }

    @Test
    void getStreet() {
        testAddress.setStreet("steenweg");

        assertEquals("steenweg", testAddress.getStreet());
    }


    @Test
    void getHouseNumber() {
        testAddress.setHouseNumber("23B");

        assertEquals("23B", testAddress.getHouseNumber());
    }


    @Test
    void getCity() {
        testAddress.setCity("Gent");

        assertEquals("Gent", testAddress.getCity());
    }


    @Test
    void getPostalCode() {
        testAddress.setPostalCode("9000");

        assertEquals("9000", testAddress.getPostalCode());
    }

    @Test
    void getBusiness() {
    }

}