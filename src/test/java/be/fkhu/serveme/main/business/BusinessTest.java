package be.fkhu.serveme.main.business;

import be.fkhu.serveme.main.booking.Booking;
import be.fkhu.serveme.main.menu_item.MenuItem;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class BusinessTest {
    Business businessTest = new Business();
    Set<MenuItem> menuItemsTest = new Set<MenuItem>() {
        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @Override
        public Iterator<MenuItem> iterator() {
            return null;
        }

        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return null;
        }

        @Override
        public boolean add(MenuItem menuItem) {
            return false;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean addAll(Collection<? extends MenuItem> c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            return false;
        }

        @Override
        public void clear() {

        }
    };
    Set<Booking> bookingsTest = new Set<Booking>() {
        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @Override
        public Iterator<Booking> iterator() {
            return null;
        }

        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return null;
        }

        @Override
        public boolean add(Booking booking) {
            return false;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean addAll(Collection<? extends Booking> c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            return false;
        }

        @Override
        public void clear() {

        }
    };

    {
        businessTest.setId(2L);
        businessTest.setName( "Curry Cravings" );
        businessTest.setDescription( "Alle curries waarvan je droomt." );
        businessTest.setMenuItems( menuItemsTest );
        businessTest.setBookings( bookingsTest );

    }

    @Test
    void getId() {
        assertEquals( java.util.Optional.of( 2L ), java.util.Optional.ofNullable( businessTest.getId() ));
    }

    @Test
    void getName() {
        assertEquals( "Curry Cravings", businessTest.getName() );
    }

    @Test
    void getDescription() {
        assertEquals( "Alle curries waarvan je droomt.", businessTest.getDescription() );
    }

    @Test
    void getMenuItems() { assertEquals(businessTest.getMenuItems(), menuItemsTest ); }

    @Test
    void getBookings() { assertEquals( businessTest.getBookings(), bookingsTest );
    }

    @Test
    void addBooking() {
    }

    @Test
    void addMenuItem() {
    }

    @Test
    void testToString() {
    }


}