package be.fkhu.serveme.main.category;

import be.fkhu.serveme.main.menu_item.MenuItem;
import org.junit.Test;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.assertEquals;

class CategoryTest {
    Category categoryTest = new Category();
    Set<MenuItem> menuItemsTest = new Set<MenuItem>() {
        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @Override
        public Iterator<MenuItem> iterator() {
            return null;
        }

        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return null;
        }

        @Override
        public boolean add(MenuItem menuItem) {
            return false;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean addAll(Collection<? extends MenuItem> c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            return false;
        }

        @Override
        public void clear() {

        }
    };

    {
        categoryTest.setId(2L);
        categoryTest.setName( "vegetarisch" );
        categoryTest.setMenuItems( menuItemsTest );
    }

    @Test
    void getId() {
        assertEquals( java.util.Optional.ofNullable( categoryTest.getId()), java.util.Optional.of( 2L ));
    }

    @Test
    void getName() {
        assertEquals(categoryTest.getName(),  "vegetarisch" );
    }

    @Test
    void getMenuItems() {
        assertEquals( categoryTest.getMenuItems(), menuItemsTest );
    }


}