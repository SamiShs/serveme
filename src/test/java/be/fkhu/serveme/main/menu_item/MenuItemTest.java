package be.fkhu.serveme.main.menu_item;


import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class MenuItemTest {
    MenuItem menuItemTest = new MenuItem();
    {
        menuItemTest.setId( 2L );
        menuItemTest.setName( "Groene curry met bamboespruiten" );
        menuItemTest.setPrice( 12.75 );
        menuItemTest.setDescription( "Een milde curry met  aromatische kruiding en veel groenten, authentiek recept." );
    }



    @DisplayName( "Test MenuItem.getId()" )
    @Test
    void getIdTest() {
        assertEquals( java.util.Optional.ofNullable( menuItemTest.getId()), java.util.Optional.of( 2L ));
    }


    @DisplayName( "Test MenuItem.getName()" )
    @Test
    void getNameTest() {
       assertEquals( menuItemTest.getName(),"Groene curry met bamboespruiten");
    }

    @DisplayName( "Test MenuItem.getPrice()" )
    @Test
    void getPriceTest() {
        assertEquals(  menuItemTest.getPrice(),12.75 );
    }

    @DisplayName( "Test MenuItem.getDescription()" )
    @Test
    void getDescriptionTest() {
        assertEquals( menuItemTest.getDescription(), "Een milde curry met  aromatische kruiding en veel groenten, authentiek recept." );
    }

//    @DisplayName( "Test MenuItem.getCategories()" )
//    @Test
//    void getCategories() {
//    }



}