package be.fkhu.serveme.main.tables;

import be.fkhu.serveme.main.booking.Booking;
import be.fkhu.serveme.main.order.Order;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.assertEquals;

class TableTest {


    Table tableTest = new Table();
    Set<Order> ordersTest =  new Set<Order>() {
        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @Override
        public Iterator<Order> iterator() {
            return null;
        }

        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return null;
        }

        @Override
        public boolean add(Order order) {
            return false;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean addAll(Collection<? extends Order> c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            return false;
        }

        @Override
        public void clear() {
        }
    };
    Booking bookingTest = new Booking(  ) ;

    {
//        tableTest.setOrders( ordersTest );
//        tableTest.setBookings(bookingTest);
//        tableTest.setTable_number( 2 );
    }

//    @Test
//    void getOrders() {
//        assertEquals( tableTest.getOrders(), ordersTest);
//    }

    @Test
    void getBookings() {
        assertEquals(tableTest.getBookings(), bookingTest );
    }


//    @Test
//    void addOrder() {
//    }

    @Test
    void getTableNumber() {
//       assertEquals( tableTest.getTable_number(), 2 );
    }

//    @Test
//    void testToString() {
//    }
//
//    @Test
//    void testEquals() {
//    }
//
//    @Test
//    void testHashCode() {
//    }


}