package be.fkhu.serveme.main.booking;

import be.fkhu.serveme.account.user.User;
import be.fkhu.serveme.main.business.Business;
import be.fkhu.serveme.main.tables.Table;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class BookingTest {

    Booking bookingTest = new Booking(  );
    User userTest = new User();
    Business businessTest = new Business();
    Set <Table> tableTest = new Set<Table>() {
        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @Override
        public Iterator<Table> iterator() {
            return null;
        }

        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return null;
        }

        @Override
        public boolean add(Table table) {
            return false;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean addAll(Collection<? extends Table> c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            return false;
        }

        @Override
        public void clear() {

        }
    };

    {
        bookingTest.setUser( userTest );
        bookingTest.setAmountOfPersons( 5 );
       // bookingTest.setTimeAtRestaurant(22-05-2021 );
        bookingTest.setBusiness( businessTest );
        bookingTest.setTables( tableTest );
    }

    @Test
    void getUser() { assertEquals(bookingTest.getUser(), userTest );
    }

    @Test
    void getAmountOfPersons() {
        assertEquals( bookingTest.getAmountOfPersons(), 5 );
    }

//    @Test
//    void getTimeAtRestaurant() {
//    }

//    @Test
//    void getId() {
//    }

    @Test
    void getBusiness() {
            assertEquals( bookingTest.getBusiness(), businessTest );
    }

    @Test
    void getTables() {
        assertEquals( bookingTest.getTables(),tableTest );
    }

//    @Test
//    void addTable() {
//    }

//    @Test
//    void testToString() {
//    }
}