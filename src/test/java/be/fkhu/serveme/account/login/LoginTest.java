package be.fkhu.serveme.account.login;

import be.fkhu.serveme.account.user.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LoginTest {

    Login loginTest = new Login();

    User userTest = new User();


    @Test
    void getEmail() {
        loginTest.setEmail("mauro123@gmail.com");

        assertEquals("mauro123@gmail.com", loginTest.getEmail());
    }


    @Test
    void getPassWord() {
        loginTest.setPassWord("lmao");

        assertEquals("lmao", loginTest.getPassWord());
    }


    @Test
    void getUser() {
        loginTest.setUser(userTest);

        assertEquals(userTest, loginTest.getUser());
    }

}