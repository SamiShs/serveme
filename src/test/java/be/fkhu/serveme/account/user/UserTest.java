package be.fkhu.serveme.account.user;

import be.fkhu.serveme.account.login.Login;
import be.fkhu.serveme.main.address.Address;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    User testUser = new User();
    Address testAddress = new Address();
    Login testLogin = new Login();



    @Test
    void getUser_ID() {
        testUser.setUser_ID(2);

        assertEquals(2, testUser.getUser_ID());
    }


    @Test
    void getName() {
        testUser.setName("Mauro");

        assertEquals("Mauro", testUser.getName());
    }


    @Test
    void getFamilyName() {
        testUser.setFamilyName("De Groote");

        assertEquals("De Groote", testUser.getFamilyName());
    }



    @Test
    void getAddress() {
        testUser.setAddress(testAddress);

        assertEquals(testAddress, testUser.getAddress());

    }


    @Test
    void getLogin() {
        testUser.setLogin(testLogin);

        assertEquals(testLogin, testUser.getLogin());
    }


    @Test
    void getUserBookings() {
    }


    @Test
    void addUserBooking() {
    }

    @Test
    void getRole() {
    }


}