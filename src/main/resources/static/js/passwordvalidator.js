
function displaydiv(){
    document.getElementById("message").style.display = "block";
}

function validate()  {
    var lowerCaseLetters = /[a-z]/g;

    if(document.getElementById("password").value.match(lowerCaseLetters)) {
        document.getElementById("letter").classList.remove("invalid");
        document.getElementById("letter").classList.add("valid");
    } else {
        document.getElementById("letter").classList.remove("valid");
        document.getElementById("letter").classList.add("invalid");
    }


    var upperCaseLetters = /[A-Z]/g;
    if(document.getElementById("password").value.match(upperCaseLetters)) {
        document.getElementById("capital").classList.remove("invalid");
        document.getElementById("capital").classList.add("valid");
    } else {
        document.getElementById("capital").classList.remove("valid");
        document.getElementById("capital").classList.add("invalid");
    }


    var numbers = /[0-9]/g;
    if(document.getElementById("password").value.match(numbers)) {
         document.getElementById("number").classList.remove("invalid");
         document.getElementById("number").classList.add("valid");
    } else {
         document.getElementById("number").classList.remove("valid");
         document.getElementById("number").classList.add("invalid");
    }


    if(document.getElementById("password").value.length >= 8) {
        document.getElementById("length").classList.remove("invalid");
        document.getElementById("length").classList.add("valid");
    } else {
        document.getElementById("length").classList.remove("valid");
        document.getElementById("length").classList.add("invalid");
    }

    if(document.getElementById("password").value.match(lowerCaseLetters) &&
        document.getElementById("password").value.match(upperCaseLetters) &&
        document.getElementById("password").value.match(numbers) &&
        document.getElementById("password").value.length >= 8){
        document.getElementById("message").style.display = "none";
    }else{
        document.getElementById("message").style.display = "block";
    }

}

