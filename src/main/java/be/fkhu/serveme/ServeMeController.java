//package be.fkhu.serveme;
//
//import be.fkhu.serveme.account.AccountController;
//import be.fkhu.serveme.account.LoginRequest;
//import be.fkhu.serveme.account.RegisterRequest;
//import be.fkhu.serveme.account.user.Role;
//import be.fkhu.serveme.account.user.User;
//import be.fkhu.serveme.main.booking.Booking;
//import be.fkhu.serveme.main.booking.BookingRequestHelper;
//import be.fkhu.serveme.main.business.Business;
//import be.fkhu.serveme.main.business.BusinessController;
//import be.fkhu.serveme.main.business.SearchRequest;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.*;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpSession;
//import java.time.LocalDate;
//import java.time.LocalTime;
//import java.time.format.DateTimeFormatter;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Objects;
//import java.util.Set;
//
//
///**
// * The type Serve me controller.
// *
// * @author Samirullah
// */
//@Controller
//public class ServeMeController {
//
//    private final String VIEW_HOME = "index";
//    private final String ACCOUNT = "account/";
//    private final String VIEW_LOGIN = "login";
//    private final String VIEW_REGISTER = "register";
//    private final String USER_PANEL = "userPanel/";
//    private final String VIEW_USER = "user";
//    private final String VIEW_SEARCH_RESTAURANT = "searchRestaurant";
//    private final String BOOKING = "booking/";
//    private final String VIEW_BOOKING = "bookings";
//    private final String VIEW_NEWBOOKING = "addNewBooking";
//    private final String VIEW_RESTAURANT = "businesses";
//
//    private final AccountController accountController;
//    private final BusinessController businessController;
//
//    /**
//     * Instantiates a new Serve me controller.
//     *
//     * @param accountController  the account controller
//     * @param businessController the business controller
//     */
//    public ServeMeController(AccountController accountController, BusinessController businessController) {
//        this.accountController = accountController;
//        this.businessController = businessController;
//    }
//
//    /**
//     * The get request for login.
//     * If user is already logged in it returns the user panel view.
//     *      *
//     * @param session the session is checked if it contains authorization
//     * @param model   the model is given the login request object, on which Thymeleaf maps the login form
//     * @return the login view
//     */
//    @GetMapping(VIEW_LOGIN)
//    public String getLogin(HttpSession session, Model model) {
//        if (isSessionAuthorized(session)) {
//            return userPanel(session);
//        }
//
//        LoginRequest loginRequest = new LoginRequest();
//        model.addAttribute("loginRequest", loginRequest);
//
//        return ACCOUNT+VIEW_LOGIN;
//    }
//
//    /**
//     * The backend accountController is used for performing login
//     * If no login found a message is passed  to the view
//     *
//     * @param session            the session used for passing the user
//     * @param httpServletRequest the http servlet request is used for setting the authentication
//     * @param loginRequest       the login request
//     * @param model              the model
//     * @return the string is a redirect to the page from which login is called
//     */
//
//    @PostMapping(VIEW_LOGIN)
//    public String logInAccount(HttpSession session, HttpServletRequest httpServletRequest, @ModelAttribute("loginRequest") LoginRequest loginRequest, Model model) {
//        if (isSessionAuthorized(session)) {
//            return userPanel(session);
//        }
//
//        ResponseEntity<User> responseEntity = accountController.loginAccount(httpServletRequest, loginRequest);
//        User user = responseEntity.getBody();
//        if (user == null) {
//            model.addAttribute("loginError", "user not found");
//            return ACCOUNT+VIEW_LOGIN;
//        }
//        session.setAttribute("user", user);
//
//        String redirectUrl = Objects.isNull(session.getAttribute("redirectUrl")) ? VIEW_HOME : (String) session.getAttribute("redirectUrl");
//        return "redirect:" + redirectUrl;
//
//    }
//
//
//    /**
//     * Gets register request.
//     *
//     * @param session the session
//     * @param model   the model
//     * @return the register
//     */
//    @GetMapping(VIEW_REGISTER)
//    public String getRegister(HttpSession session,Model model) {
//        if (isSessionAuthorized(session)) {
//            return userPanel(session);
//        }
//        model.addAttribute("cities", vlaamseSteden);
//
//        RegisterRequest registerRequest = new RegisterRequest();
//        model.addAttribute("registerRequest", registerRequest);
//
//        return ACCOUNT+VIEW_REGISTER;
//    }
//
//    /**
//     * Create account.
//     *
//     * @param httpServletRequest the http servlet request
//     * @param registerRequest    the register request
//     * @param session            the session
//     * @return the string
//     */
////    @PostMapping(VIEW_REGISTER)
////    public String createAccount(HttpServletRequest httpServletRequest, @ModelAttribute("registerRequest") RegisterRequest registerRequest, HttpSession session,Model model) {
////
////        if (!isSessionAuthorized(session)) {
////
////            ResponseEntity<User> responseEntity = accountController.createAccount(httpServletRequest,registerRequest);
////            User user = (User) responseEntity.getBody();
////            if (Objects.isNull(user)) {
////                model.addAttribute("messageToRegister","user with this username already exists!");
////                return getRegister(session,model);
////            }
////            session.setAttribute("user", responseEntity.getBody());
////
////        }
////        return userPanel(session);
////    }
//
//
//    /**
//     * User panel.
//     *
//     * @param session the session
//     * @return the string
//     */
//    @GetMapping(USER_PANEL)
//    public String userPanel(HttpSession session) {
//        if (!isSessionAuthorized(session)) {
//            return VIEW_HOME;
//        }
//
//        User user = (User) session.getAttribute("user");
//        if (user.getRole().equals(Role.ROLE_MANAGER)) {
//            session.setAttribute("manager", true);
//        }else {
//            session.setAttribute("manager", false);
//        }
//
//        return USER_PANEL+VIEW_USER;
//    }
//
//
//    /**
//     * Show bookings.
//     *
//     * @param model   the model
//     * @param session the session
//     * @return the string
//     */
//    @GetMapping(VIEW_BOOKING)
//    public String showBooking(Model model, HttpSession session) {
//        if (!isSessionAuthorized(session)) {
//            return userPanel(session);
//        }
//
//        User user = (User) session.getAttribute("user");
//        ResponseEntity<Set<Booking>> userBookings = businessController.getBookings(user.getUser_ID() + "");
//        model.addAttribute("userBookings", userBookings.getBody());
//        return BOOKING+VIEW_BOOKING;
//    }
//
//
//    /**
//     * New booking request.
//     *
//     * @param model   the model
//     * @param session the session
//     * @return the string
//     */
//    @GetMapping(VIEW_NEWBOOKING)
//    public String NewBooking(Model model, HttpSession session) {
//        session.setAttribute("redirectUrl",VIEW_NEWBOOKING);
//        String businessID = (String) session.getAttribute("businessID");
//        if (Objects.isNull(businessID)) {
//            return "redirect:businesses";
//        }
//
//        User user = (User) session.getAttribute("user");
//
//        if (Objects.isNull(user) || user.getUser_ID() == -1) {
//            user = new User();
//            user.setUser_ID(-1);
//            session.setAttribute("user", user);
//        }
//
//        BookingRequestHelper bookingRequestHelper = new BookingRequestHelper();
//        bookingRequestHelper.setUserID(user.getUser_ID() + "");
//        bookingRequestHelper.setRestaurantID(businessID);
//
//        model.addAttribute("bookingRequestHelper", new BookingRequestHelper());
//        List<String> nextTwoWeeks = new ArrayList<>();
//        List<String> openingHours = new ArrayList<>();
//        LocalDate localDate = LocalDate.now();
//        LocalTime localTime = LocalTime.parse("14:00:00", DateTimeFormatter.ISO_LOCAL_TIME);
//        for (int i = 0; i < 14; i++) {
//            nextTwoWeeks.add(localDate.plusDays(i).format(DateTimeFormatter.ISO_LOCAL_DATE));
//            openingHours.add(localTime.plusMinutes(i * 30).format(DateTimeFormatter.ISO_LOCAL_TIME).substring(0,5));
//
//        }
//        model.addAttribute("nextTwoWeeks", nextTwoWeeks);
//        model.addAttribute("openingHours", openingHours);
//        return BOOKING+VIEW_NEWBOOKING;
//    }
//
//    /**
//     * Add new booking request.
//     *
//     * @param bookingRequestHelper the booking request helper
//     * @param model                the model
//     * @param session              the session
//     * @return the string
//     */
//    @PostMapping(VIEW_NEWBOOKING)
//    public String addNewBooking(@ModelAttribute("newBookingRequest") BookingRequestHelper bookingRequestHelper, Model model, HttpSession session) {
//
//        if (!isSessionAuthorized(session) || bookingRequestHelper.getUserID().equals("-1")) {
//            model.addAttribute("messageToLogin", "please login before you can make reservation");
//            return getLogin(session, model);
//        }
//
//        ResponseEntity<String> responseEntity = businessController.addBooking(bookingRequestHelper);
//        session.removeAttribute("businessID");
//
//
//        return showBooking(model, session);
//    }
//
//    @GetMapping(VIEW_SEARCH_RESTAURANT)
//    public String searchRestaurant(Model model, @RequestParam(defaultValue = "all") String ct, @RequestParam(defaultValue = "all") String sch) {
//        SearchRequest searchRequest = new SearchRequest();
//        searchRequest.setCity(ct);
//        searchRequest.setSearchString(sch);
//        System.out.println("searchRequest: " +searchRequest.getSearchString()+"  " +searchRequest.getCity());
//
//
//        ResponseEntity<List<Business>> responseEntity = businessController.searchBusiness(searchRequest);
//        System.out.println("pppppppppppppppppppppp "+responseEntity.getBody());
//        model.addAttribute("searchRequest", searchRequest);
//        model.addAttribute("businesses", responseEntity.getBody());
//        model.addAttribute("cities", vlaamseSteden);
//        return VIEW_SEARCH_RESTAURANT;
//    }
//
//    @PostMapping(VIEW_SEARCH_RESTAURANT)
//    public String search(@ModelAttribute("searchRequest") SearchRequest searchRequest, Model model) {
//        return "redirect:" + VIEW_SEARCH_RESTAURANT + "?ct=" + searchRequest.getCity() + "&sch=" + searchRequest.getSearchString();
//    }
//
//
//    /**
//     * home page request.
//     *
//     * @param session the session
//     * @return the index
//     */
//    @GetMapping({VIEW_HOME,"/"})
//    public String getIndex(HttpSession session) {
//        session.setAttribute("redirectUrl","/index");
//        if (isSessionAuthorized(session)) {
//            System.out.println("user is logged in so no standard home");
//            return USER_PANEL+VIEW_USER;
//        }
//        return VIEW_HOME;
//    }
//
//
//    /**
//     * Log out .
//     *
//     * @param httpServletRequest the http servlet request
//     * @return the string
//     */
//    @GetMapping("/logout")
//    public String logOut(HttpServletRequest httpServletRequest) {
//        try {
//            httpServletRequest.logout();
//        } catch (Exception e) {
//            return VIEW_HOME;
//        }
//
//        ;
//        return "redirect:/auth/logout";
//    }
//
//    /**
//     * Checks if users session is authorized.
//     *
//     * @param session the session
//     * @return the boolean
//     */
//    boolean isSessionAuthorized(HttpSession session) {
//
//        return accountController.isSessionAuthorized(session);
//    }
//
//    String[] vlaamseSteden = {"Aalst",
//            "Aarschot",
//            "Antwerpen",
//            "Beringen",
//            "Bilzen",
//            "Blankenberge",
//            "Borgloon",
//            "Bree",
//            "Brugge",
//            "Damme",
//            "Deinze",
//            "Dendermonde",
//            "Diest",
//            "Diksmuide",
//            "Dilsen-Stokkem",
//            "Eeklo",
//            "Geel",
//            "Genk",
//            "Gent",
//            "Geraardsbergen",
//            "Gistel",
//            "Halen",
//            "Halle",
//            "Hamont-Achel",
//            "Harelbeke",
//            "Hasselt",
//            "Herentals",
//            "Herk-de-Stad",
//            "Hoogstraten",
//            "Ieper",
//            "Izegem",
//            "Kortrijk",
//            "Landen",
//            "Leuven",
//            "Lier",
//            "Lokeren",
//            "Lommel",
//            "Lo-Reninge",
//            "Maaseik",
//            "Mechelen",
//            "Menen",
//            "Mesen",
//            "Mortsel",
//            "Nieuwpoort",
//            "Ninove",
//            "Oostende",
//            "Oudenaarde",
//            "Oudenburg",
//            "Peer",
//            "Poperinge",
//            "Roeselare",
//            "Ronse",
//            "Scherpenheuvel-Zichem",
//            "Sint-Niklaas",
//            "Sint-Truiden",
//            "Tielt",
//            "Tienen",
//            "Tongeren",
//            "Torhout",
//            "Turnhout",
//            "Veurne",
//            "Vilvoorde",
//            "Waregem",
//            "Waregem",
//            "Wervik",
//            "Zottegem",
//            "Zoutleeuw"};
//}
