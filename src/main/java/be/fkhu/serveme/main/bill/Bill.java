package be.fkhu.serveme.main.bill;

import be.fkhu.serveme.account.user.User;
import be.fkhu.serveme.main.business.Business;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="Bill")
public class Bill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="Bill_ID")
    private Long id;

    @Column(name="Total_Price")
    private double totalPrice;

    @Column(name="Date")
    LocalDateTime date;

    @ManyToOne
    @JoinColumn(name="User_ID", referencedColumnName = "User_ID")
    User user;
    @ManyToOne
    private Business business;

    public Bill() {
    }


    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }
}
