package be.fkhu.serveme.main;

import be.fkhu.serveme.account.user.User;
import be.fkhu.serveme.main.business.Business;
import be.fkhu.serveme.main.category.Category;
import be.fkhu.serveme.main.menu_item.MenuItem;
import be.fkhu.serveme.main.order.Order;
import be.fkhu.serveme.main.order_menuItem.OrderMenuItem;
import be.fkhu.serveme.main.tables.Table;

import java.util.List;
import java.util.Set;

//


public interface CustomerOrderService {

    MenuItem findMenuItemById(String id);
    List<MenuItem> findAllMenuItems();
    Order createNewOrder();
    Order saveCurrentOrder(Order currentOrder);
    void saveChosenMenuItemToCurrentOrder(String menuItemId, String amount, Order currentOrder, Long sessionId);
    Set<Category> findAllCategoriesForBusiness(Long businessId);
    List<MenuItem> returnMenuItemsByCategory(String categoryName, Long businessId);

    void removeChosenMenuItemInCurrentOrder(OrderMenuItem orderMenuItemToBeRemoved, Order currentOrder);
    void saveOrderToTableTracking(Order order, Table table);         //The saveOrderToTableTracking's primary function is for the restaurant to see what table ordered what.
    void createBill(Order order, User user, Business business);      //get order, calculate price, give date, create new bill with that data and link to user.
    void removeTableInfoFromTableTracking(Table table);              //Refresh When a new booking comes in, remove previous table-info from table tracking
    String[] createPaymentRequest(String price, String businessName, String orderID) throws Exception;

    String checkPaymentStatus(String checkURL) throws  Exception;

    //BUSSINES SIDE
    //current processed orders
    //public void viewTableTracking(Business business, Table table);
    //OPTIONAL
    //Sort by category
}
