package be.fkhu.serveme.main.tables;

import javax.persistence.*;


@Entity
@javax.persistence.Table(name="Restaurant_Table_Status")
public class TableStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="Table_Status_ID")
    private Long id;

    @Column(name="Table_Status")
    @Enumerated(EnumType.STRING)
    private Status status;

    public TableStatus() {
    }

    public enum Status {
        AVAILABLE,
        RESERVED,
        OUT_OF_ORDER
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}

