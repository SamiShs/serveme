package be.fkhu.serveme.main.tables;

import be.fkhu.serveme.main.booking.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface TableRepository extends JpaRepository<Table, Long> {
//    public Table getRestaurantTableById(long id);
//    Set<Table> getRestaurantTablesByBooking(Booking booking);

}
