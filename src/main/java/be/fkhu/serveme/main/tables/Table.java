package be.fkhu.serveme.main.tables;

import be.fkhu.serveme.main.booking.Booking;
import be.fkhu.serveme.main.business.Business;
import be.fkhu.serveme.main.category.Category;
import be.fkhu.serveme.main.order.Order;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Random;
import java.util.Set;


@Entity
@javax.persistence.Table(name = "Restaurant_Table")
public class Table implements Serializable {

    //fields
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Table_ID")
    private long id;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "Restaurant_Table_Tracking",
            joinColumns = @JoinColumn(name="Table_ID"),
            inverseJoinColumns = @JoinColumn(name = "order_id"))
    private Set<Order> orders = new HashSet<>();


    @Column(name="Number_Of_Places")
    private int numberOfPlaces;
    @Column(name = "Table_Number")
    private int tableNumber;

    @ManyToOne
    @JoinColumn(name="Business_ID", referencedColumnName = "Business_ID")
    private Business business;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "Table_Booking_Tracking",
            joinColumns = @JoinColumn(name = "Table_id"),
            inverseJoinColumns = @JoinColumn(name ="Booking_id"))
    private Set<Booking> bookings = new HashSet<>();


    //empty constructor
    public Table() {
        Random random = new Random();
    }

    //getters&setters


    public long getId() {
        return id;
    }

    public int getNumberOfPlaces() {
        return numberOfPlaces;
    }

    public int getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    public void setNumberOfPlaces(int numberOfPlaces) {
        this.numberOfPlaces = numberOfPlaces;
    }

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> order) {
        this.orders = order;
    }

    public Set<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(Set<Booking> bookings) {
        this.bookings = bookings;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }
    //Methodes

    public void addOrder(Order order){
        orders.add(order);
    }

    public void addBooking(Booking booking) {
        bookings.add(booking);
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RestaurantTable{");
        sb.append("id=").append(id);
        sb.append(", order=");
        for (Order order:orders
        ) {sb.append(order.getId()).append(", ").append('\'') ;
        }
        sb.append('}');
        return sb.toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Table table = (Table) o;
        return getNumberOfPlaces() == table.getNumberOfPlaces() &&
                Objects.equals(getOrders(), table.getOrders()) &&
                Objects.equals(business, table.business);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOrders(), getNumberOfPlaces(), business);
    }
}
