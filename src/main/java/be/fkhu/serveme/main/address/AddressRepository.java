package be.fkhu.serveme.main.address;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {

    public List<Address> findAdressByStreetAndHouseNumberAndPostalCode(String street, String houseNumber, String postalCode);
    //Optional<Adress> findAdressbyId(long id);

    //void createAdress(Adress adress);
}
