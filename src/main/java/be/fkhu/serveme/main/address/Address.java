package be.fkhu.serveme.main.address;



import be.fkhu.serveme.account.user.User;
import be.fkhu.serveme.main.business.Business;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Samirullah & Mauro
 */
@Entity
@Table(name = "Address")
public class Address implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Address_ID")
    private Long address_ID;
    @Column(name = "Street")
    private String street;
    @Column(name = "Number")
    private String houseNumber;
    @Column(name = "City")
    private String city;
    @Column(name = "Postal_Code")
    private String postalCode;

    @OneToOne(mappedBy = "address")
    private Business business;



    //    @OneToMany(mappedBy = "address", fetch = FetchType.LAZY,
//            cascade = CascadeType.PERSIST)
//    private Set<User> users;

    //private String country;


    public Long getAddress_ID() {
        return address_ID;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public void setAddress_ID(Long address_ID) {
        this.address_ID = address_ID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return street.equals(address.street) &&
                houseNumber.equals(address.houseNumber) &&
                city.equals(address.city) &&
                postalCode.equals(address.postalCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(street, houseNumber, city, postalCode);
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    @Override
    public String toString() {
        return "Address{" +
                "address_ID=" + address_ID +
                ", street='" + street + '\'' +
                ", houseNumber='" + houseNumber + '\'' +
                ", city='" + city + '\'' +
                ", postalCode='" + postalCode + '\'' +
                '}';
    }
}
