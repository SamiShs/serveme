package be.fkhu.serveme.main.menu_item;

import be.fkhu.serveme.main.business.Business;
import be.fkhu.serveme.main.category.Category;
import be.fkhu.serveme.main.order.Order;
import be.fkhu.serveme.main.order_menuItem.OrderMenuItem;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import static java.util.Comparator.comparing;

@Entity
@Table(name = "MenuItem")
public class MenuItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "item_id")
    private Long id;

    @Column(name = "item_name")
    private String name;

    @Column(name = "item_price")
    private double price;

    @Column(name = "item_description")
    private String description;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "MenuItem_Category",
            joinColumns = @JoinColumn(name = "item_id"),
            inverseJoinColumns = @JoinColumn(name ="category_id"))
    private Set<Category> categories = new HashSet<>();

    @OneToMany(mappedBy = "menuItem")
    private Set<OrderMenuItem> orderMenuItems = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "business_id", referencedColumnName = "business_id")
    private Business business;

    public MenuItem(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    public void addCategory(Category category){
        this.getCategories().add(category);
        category.getMenuItems().add(this);
    }

    public Set<OrderMenuItem> getOrderMenuItems() {
        return orderMenuItems;
    }

    public void setOrderMenuItems(Set<OrderMenuItem> orderMenuItems) {
        this.orderMenuItems = orderMenuItems;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MenuItem{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", price=").append(price);
        sb.append(", description='").append(description).append('\'');
        sb.append('}');
        return sb.toString();
    }
}