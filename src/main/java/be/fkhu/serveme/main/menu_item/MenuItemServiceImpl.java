package be.fkhu.serveme.main.menu_item;

import be.fkhu.serveme.main.category.Category;
import be.fkhu.serveme.main.category.CategoryServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class MenuItemServiceImpl implements MenuItemService {

    private final MenuItemRepository menuItemRepository;

    public MenuItemServiceImpl(MenuItemRepository repository) {
        this.menuItemRepository = repository;
    }

    @Override
    public MenuItem saveMenuItem(MenuItem menuItem) {
        return menuItemRepository.save(menuItem);
    }

    @Override
    public MenuItem findMenuItemById(Long id) {
        Optional<MenuItem> optionalMenuItem = menuItemRepository.findById(id);
        if(optionalMenuItem.isEmpty()){
            throw new RuntimeException("MenuItem Not Found");
        }
        return optionalMenuItem.get();
    }

    @Override

    public List<MenuItem> findAll() {
        return menuItemRepository.findAll();
    }
}