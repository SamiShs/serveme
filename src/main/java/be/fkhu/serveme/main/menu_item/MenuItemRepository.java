package be.fkhu.serveme.main.menu_item;


import be.fkhu.serveme.main.category.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface MenuItemRepository extends JpaRepository<MenuItem, Long> {

//    @Query("SELECT '*' FROM MenuItem, Category WHERE Category.name LIKE : name")
//    List<MenuItem> findMenuItemsByCategories(String name);

}
