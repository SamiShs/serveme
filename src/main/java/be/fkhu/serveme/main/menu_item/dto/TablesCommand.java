package be.fkhu.serveme.main.menu_item.dto;

import be.fkhu.serveme.main.tables.Table;

import java.util.Set;

public class TablesCommand {

    private Set<Table> tables;

    public TablesCommand() {
    }

    public TablesCommand(Set<Table> tables) {
        this.tables = tables;
    }

    public void addTable(Table table){
        this.tables.add(table);
    }

    public Set<Table> getTables() {
        return tables;
    }

    public void setTables(Set<Table> tables) {
        this.tables = tables;
    }
}
