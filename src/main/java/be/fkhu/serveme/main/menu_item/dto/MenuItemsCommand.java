package be.fkhu.serveme.main.menu_item.dto;

import be.fkhu.serveme.main.menu_item.MenuItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MenuItemsCommand {
    private List<MenuItem> menuItems;

    public MenuItemsCommand() {
    }

    public MenuItemsCommand(Set<MenuItem> menuItems) {
        this.menuItems = new ArrayList<>(menuItems);
    }

    public void addMenuItem(MenuItem menuItem){
        this.menuItems.add(menuItem);
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }
}
