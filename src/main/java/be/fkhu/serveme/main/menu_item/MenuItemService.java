package be.fkhu.serveme.main.menu_item;

import be.fkhu.serveme.main.category.Category;

import java.util.List;

public interface MenuItemService {
    MenuItem saveMenuItem(MenuItem menuItem);
    MenuItem findMenuItemById(Long id);
    List<MenuItem> findAll();
}
