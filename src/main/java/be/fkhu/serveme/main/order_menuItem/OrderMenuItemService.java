package be.fkhu.serveme.main.order_menuItem;

import java.util.List;

public interface OrderMenuItemService {
    OrderMenuItem saveOrderMenuItem(OrderMenuItem orderMenuItem);
    OrderMenuItem findOrderMenuItemById(Long id);
    List<OrderMenuItem> findAll();
}
