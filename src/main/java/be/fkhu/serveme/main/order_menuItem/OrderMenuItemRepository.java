package be.fkhu.serveme.main.order_menuItem;


import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderMenuItemRepository extends JpaRepository<OrderMenuItem, Long> {
}
