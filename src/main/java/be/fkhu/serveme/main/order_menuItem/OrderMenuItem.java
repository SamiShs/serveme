package be.fkhu.serveme.main.order_menuItem;

import be.fkhu.serveme.main.menu_item.MenuItem;
import be.fkhu.serveme.main.order.Order;

import javax.persistence.*;
import javax.validation.constraints.Min;

@Entity
@Table(name = "Order_MenuItem")
public class OrderMenuItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="order_item_id")
    Long id;

    @Column(name="amount")
    @Min(message = "Please enter a valid amount", value = 0)
    int amount;

    @ManyToOne
    @JoinColumn(name="order_id")
    Order order;

    @ManyToOne
    @JoinColumn(name="item_id")
    MenuItem menuItem;

    @Transient
    private Long sessionId;

    public OrderMenuItem(){

    }

    public OrderMenuItem(MenuItem menuItem, int amount, Order order, Long sessionId){
        this.amount = amount;
        this.menuItem = menuItem;
        this.order = order;
        this.sessionId = sessionId;
    }


    @Transient
    public Double getTotalPrice(){
        return menuItem.getPrice() * amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public MenuItem getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(MenuItem menuItem) {
        this.menuItem = menuItem;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("OrderMenuItem{");
        sb.append("id=").append(id);
        sb.append(", amount=").append(amount);
        sb.append(", order=").append(order);
        sb.append(", menuItem=").append(menuItem);
        sb.append(", sessionId=").append(sessionId);
        sb.append('}');
        return sb.toString();
    }
}
