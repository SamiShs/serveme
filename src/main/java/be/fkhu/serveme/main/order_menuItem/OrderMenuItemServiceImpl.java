package be.fkhu.serveme.main.order_menuItem;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderMenuItemServiceImpl implements OrderMenuItemService {

    private final OrderMenuItemRepository repository;

    public OrderMenuItemServiceImpl(OrderMenuItemRepository repository) {
        this.repository = repository;
    }

    @Override
    public OrderMenuItem saveOrderMenuItem(OrderMenuItem orderMenuItem) {
        return repository.save(orderMenuItem);
    }

    @Override
    public OrderMenuItem findOrderMenuItemById(Long id) {
        Optional<OrderMenuItem> optionalOrderMenuItem = repository.findById(id);
        if(optionalOrderMenuItem.isEmpty()){
            throw new RuntimeException("OrderMenuItem not found");
        }
        return optionalOrderMenuItem.get();
    }

    @Override
    public List<OrderMenuItem> findAll() {
        return repository.findAll();
    }

}
