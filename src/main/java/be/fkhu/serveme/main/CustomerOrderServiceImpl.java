package be.fkhu.serveme.main;

import be.fkhu.serveme.account.user.User;
import be.fkhu.serveme.main.bill.Bill;
import be.fkhu.serveme.main.business.Business;
import be.fkhu.serveme.main.business.BusinessService;
import be.fkhu.serveme.main.category.Category;
import be.fkhu.serveme.main.menu_item.MenuItem;
import be.fkhu.serveme.main.menu_item.MenuItemService;
import be.fkhu.serveme.main.order.Order;
import be.fkhu.serveme.main.order.OrderService;
import be.fkhu.serveme.main.order_menuItem.OrderMenuItem;
import be.fkhu.serveme.main.tables.Table;
import okhttp3.*;
import org.apache.tomcat.util.json.JSONParser;
import org.springframework.stereotype.Service;


import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CustomerOrderServiceImpl implements CustomerOrderService {

    private final MenuItemService menuItemService;
    private final OrderService orderService;
    private final BusinessService businessService;

    public CustomerOrderServiceImpl(MenuItemService menuItemService, OrderService orderService, BusinessService businessService) {
        this.menuItemService = menuItemService;
        this.orderService = orderService;
        this.businessService = businessService;
    }

    @Override
    public Order createNewOrder() {
        return new Order();
    }

    @Override
    public List<MenuItem> findAllMenuItems() {
        return menuItemService.findAll();
    }

    @Override
    public MenuItem findMenuItemById(String id) {
        return menuItemService.findMenuItemById(Long.valueOf(id));
    }

    @Override
    public void saveChosenMenuItemToCurrentOrder(String menuItemId, String amount, Order currentOrder, Long sessionId) {
        MenuItem menuItem = findMenuItemById(menuItemId);
        OrderMenuItem orderMenuItem = new OrderMenuItem(menuItem,Integer.parseInt(amount), currentOrder, sessionId);
        currentOrder.addOrderMenuItemToSet(orderMenuItem);
    }

    @Override
    public Order saveCurrentOrder(Order currentOrder) {
        return orderService.saveOrder(currentOrder);
    }

    @Override
    public void removeChosenMenuItemInCurrentOrder(OrderMenuItem orderMenuItemToBeRemoved, Order currentOrder) {
        currentOrder.getOrderMenuItems().remove(orderMenuItemToBeRemoved);
    }

    @Override
    public Set<Category> findAllCategoriesForBusiness(Long businessId) {
        Set<MenuItem> menuItems = businessService.findBusinessById(businessId).getMenuItems();
//        Set<MenuItem> menuItems = businessService.findBusinessById(1L).getMenuItems();
        Set<Category> businessCategories = new HashSet<>();
        for (MenuItem menuItem : menuItems) {
            businessCategories.addAll(menuItem.getCategories());
        }
        return businessCategories;
    }

    @Override
    public List<MenuItem> returnMenuItemsByCategory(String categoryName, Long businessId) {
        Set<MenuItem> menuItems = businessService.findBusinessById(businessId).getMenuItems();
//        Set<MenuItem> menuItems = businessService.findBusinessById(1L).getMenuItems();
              List<MenuItem> menuItemsByCategory =
                      menuItems.stream().filter(menuItem -> {
            for (Category category : menuItem.getCategories()) {
            if(category.getName().equals(categoryName)) return true;
            }
            return false;
            }).collect(Collectors.toList());
        return menuItemsByCategory;
    }

    @Override
    public void saveOrderToTableTracking(Order order, Table table) {
//        table.addOrder(order);
        //order.addTableToOrder(table);
    }

    @Override
    public void createBill(Order order, User user, Business business) {
        Bill bill = new Bill();
        bill.setUser(user);
        bill.setBusiness(business);
        bill.setTotalPrice(order.getTotalOrderPrice());
        bill.setDate(LocalDateTime.now());
    }

    @Override
    public void removeTableInfoFromTableTracking(Table table) {

    }

    public String[] createPaymentRequest(String price,String businessName, String orderID) throws Exception{
        price = String.format("%.2f", Float.parseFloat(price)).replace(",",".");
        System.out.println(price);
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "{\"amount\":{\"currency\": \"EUR\", \"value\": \""+price+"\"}, \"description\": \"Order #"+orderID+" at "+businessName+"\", \"redirectUrl\": \"http://localhost:8080/paymentCheck\", \"method\":\"bancontact\"}");
        Request request = new Request.Builder()
                .url("https://api.mollie.com/v2/payments")
                .method("POST", body)
                .addHeader("Authorization", "Bearer test_b2fexTUBxFQ8k7nvcpDATE7Mc9EJdy")
                .addHeader("Content-Type", "application/json")
                .build();
        Response response = client.newCall(request).execute();
        JSONParser obj = new JSONParser(response.body().string());
        Map<String, Object> responseMap = obj.parseObject();
        System.out.println("printing returned payment " +responseMap);
        Map<String, Object> links = (Map<String, Object>) responseMap.get("_links");
        System.out.println("links printing "+links  );
        System.out.println("links printing "+links.keySet());

        Map<String, Object> linkSelf = (Map<String, Object>) links.get("self");
        System.out.println("linkself printing "+linkSelf);
        Map<String, Object> linkCheckouts = (Map<String, Object>) links.get("checkout");
        System.out.println(linkCheckouts);
        String linkCheckPay = (String) linkSelf.get("href");
        String linkCheckout = (String) linkCheckouts.get("href");
        return new String[]{linkCheckout, linkCheckPay};

    }

    @Override
    public String checkPaymentStatus(String checkURL) throws Exception{
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(checkURL)
                .method("GET", null)
                .addHeader("Authorization", "Bearer test_b2fexTUBxFQ8k7nvcpDATE7Mc9EJdy")
                .build();
        Response response = client.newCall(request).execute();
        JSONParser obj = new JSONParser(response.body().string());
        Map<String, Object> responseMap = obj.parseObject();
        String  statusCheck = (String) responseMap.get("status");
        return statusCheck;

    }
    //public String onOrderConfirmRedirectToBill(@ModelAttribute("CurrentOrder") Order order)
}
