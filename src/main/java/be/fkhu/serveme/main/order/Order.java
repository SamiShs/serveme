package be.fkhu.serveme.main.order;

import be.fkhu.serveme.main.order_menuItem.OrderMenuItem;
import be.fkhu.serveme.main.tables.Table;
import be.fkhu.serveme.main.menu_item.MenuItem;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@javax.persistence.Table(name = "Orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    private Long id;

    @Column(name = "order_time")
    private LocalDateTime time;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private Set<OrderMenuItem> orderMenuItems = new HashSet<>();


    //TODO Fix Link with table
//    @ManyToMany(cascade = CascadeType.ALL)
//    @JoinTable(name = "Restaurant_Table_Tracking",
//            joinColumns = @JoinColumn(name="order_id"),
//            inverseJoinColumns = @JoinColumn(name = "Table_ID"))
//    private Set<Table> tables = new HashSet<>();

    public Order(){

    }

    @Transient
    public Double getTotalOrderPrice(){
        double sum = 0D;
        Set<OrderMenuItem> orders = getOrderMenuItems();
        for (OrderMenuItem o : orders){
            sum += o.getTotalPrice();
        }
        return sum;
    }

    @Transient
    public boolean hasOrderMenuItems(){
        return !getOrderMenuItems().isEmpty();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public Set<OrderMenuItem> getOrderMenuItems() {
        return orderMenuItems;
    }

    public void setOrderMenuItems(Set<OrderMenuItem> orderMenuItems) {
        this.orderMenuItems = orderMenuItems;
    }

//    public Set<Table> getTables() {
//        return tables;
//    }
//
//    public void setTables(Set<Table> table) {
//        this.tables = table;
//    }

    public void addOrderMenuItemSet(Set<OrderMenuItem> set){
        for (OrderMenuItem orderMenuItem : set) {
            orderMenuItem.setOrder(this);
        }
    }

    public void addOrderMenuItemToSet(OrderMenuItem orderMenuItem){
        orderMenuItems.add(orderMenuItem);
    }

//    public void addTableToOrder(Table table){
//        tables.add(table);}

}
