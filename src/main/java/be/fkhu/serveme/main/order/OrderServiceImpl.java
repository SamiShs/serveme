package be.fkhu.serveme.main.order;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository repository;

    public OrderServiceImpl(OrderRepository repository) {
        this.repository = repository;
    }

    @Override
    public Order saveOrder(Order order) {
        return repository.save(order);
    }

    @Override
    public Order findOrderById(Long id) {
        Optional<Order> orderOptional = repository.findById(id);
        if(orderOptional.isEmpty()){
            throw new RuntimeException("Order Not Found");
        }
        return orderOptional.get();
    }

    @Override
    public List<Order> findAll() {
        return repository.findAll();
    }
}