package be.fkhu.serveme.main.order;

import java.util.List;

public interface OrderService {
    Order saveOrder(Order order);
    Order findOrderById(Long id);
    List<Order> findAll();
}
