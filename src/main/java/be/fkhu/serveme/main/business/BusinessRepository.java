package be.fkhu.serveme.main.business;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface BusinessRepository extends JpaRepository<Business, Long> {
    List<Business> findBusinessByNameContainingOrDescriptionContaining(String name, String description);
}
