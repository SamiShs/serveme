package be.fkhu.serveme.main.business;

import be.fkhu.serveme.main.booking.Booking;
import be.fkhu.serveme.main.booking.BookingRequestHelper;

import java.util.List;
import java.util.Set;

public interface BusinessService {
    Business safeBusiness(Business business);
    Business findBusinessById(Long id);
    List<Business> findAllBusinesses();

    List<Business> searchBusiness(SearchRequest searchRequest);
}
