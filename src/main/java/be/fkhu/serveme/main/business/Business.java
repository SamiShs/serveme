package be.fkhu.serveme.main.business;

import be.fkhu.serveme.account.login.Login;
import be.fkhu.serveme.main.address.Address;
import be.fkhu.serveme.main.bill.Bill;
import be.fkhu.serveme.main.booking.Booking;
import be.fkhu.serveme.main.menu_item.MenuItem;
import be.fkhu.serveme.main.tables.Table;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@javax.persistence.Table(name = "Business")
public class Business {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Business_ID")
    private Long id;

    @Column(name = "Business_name")
    private String name;

    @Column(name = "Description")
    private String description;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "business", fetch = FetchType.EAGER)
    private Set<MenuItem> menuItems = new HashSet<>();

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "Address_ID", referencedColumnName = "Address_ID")
    private Address address;

    @OneToMany(mappedBy = "business", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private Set<Booking> bookings = new HashSet<>();

    @OneToMany(mappedBy = "business", cascade=CascadeType.PERSIST,fetch = FetchType.EAGER)
    private Set<Table> tables = new HashSet<>();

    @OneToMany(mappedBy = "business")
    private Set<Bill> bills;

    @Column(name="Timeslot")
    private int timeSlot;


    public Business(){

    }

    public Set<Table> getTables() {
        return tables;
    }

    public void setTables(Set<Table> tables) {
        this.tables = tables;
    }

    public int getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(int timeSlot) {
        this.timeSlot = timeSlot;
    }

    public void setAddress(Address address) {
        this.address = address;
        address.setBusiness(this);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(Set<MenuItem> menuItems) {

        this.menuItems = menuItems;
    }

    public void setBookings(Set<Booking> bookings) {
        this.bookings = bookings;
    }

    public Set<Booking> getBookings() {
        return bookings;
    }

    public void addBooking(Booking booking) {
        bookings.add(booking);
    }

    public Set<Bill> getBills() {
        return bills;
    }

    public void setBills(Set<Bill> bills) {
        this.bills = bills;
    }

    public void addMenuItem(MenuItem item){
        this.getMenuItems().add(item);
        item.setBusiness(this);
    }
    public void addTable(Table table) {
        getTables().add(table);
        table.setBusiness(this);
    }

    public Address getAddress() {
        return address;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Business{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", address=").append(address).append('\'');
        sb.append(", description='").append(description).append('\'');
//        for (MenuItem menuItem : menuItems) {
//            sb.append(menuItem).append('\'');
//        }
//        for (Table table : tables) {
//            sb.append(table).append('\'');
//        }
        sb.append('}');
        return sb.toString();
    }


}
