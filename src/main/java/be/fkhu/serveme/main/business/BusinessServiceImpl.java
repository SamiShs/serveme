package be.fkhu.serveme.main.business;

import be.fkhu.serveme.account.user.User;
import be.fkhu.serveme.account.user.UserRepository;
import be.fkhu.serveme.main.booking.Booking;
import be.fkhu.serveme.main.booking.BookingRepository;
import be.fkhu.serveme.main.booking.BookingRequestHelper;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Service
public class BusinessServiceImpl implements BusinessService {

    private final BusinessRepository businessRepository;
    private final UserRepository userRepository;
    private final BookingRepository bookingRepository;

    public BusinessServiceImpl(BusinessRepository businessRepository, UserRepository userRepository, BookingRepository bookingRepository) {
        this.businessRepository = businessRepository;
        this.userRepository = userRepository;
        this.bookingRepository = bookingRepository;
    }

    @Override
    public Business safeBusiness(Business business) {
        Business savedBusiness = businessRepository.save(business);
        return savedBusiness;
    }

    @Override
    public Business findBusinessById(Long id) {
        Optional<Business> optionalBusiness = businessRepository.findById(id);
        if(optionalBusiness.isEmpty()){
            throw new RuntimeException("Business not found");
        }
        return optionalBusiness.get();
    }

    @Override
    public List<Business> findAllBusinesses() {
        return businessRepository.findAll();
    }


    @Override
    public List<Business> searchBusiness(SearchRequest searchRequest) {
        List<Business> businesses;
        if (Objects.isNull(searchRequest.getSearchString())||searchRequest.getSearchString().isEmpty()) {
            businesses = businessRepository.findAll();
        }else {
            businesses = businessRepository.findBusinessByNameContainingOrDescriptionContaining(
                    searchRequest.getSearchString(), searchRequest.getSearchString());
        }

        if (Objects.isNull(searchRequest.getCity()) || searchRequest.getCity().isEmpty()) {
            return businesses;
        }else {
            businesses.removeIf(business -> !(business.getAddress().getCity().equalsIgnoreCase(searchRequest.getCity())));
        }
        return businesses;
    }

}
