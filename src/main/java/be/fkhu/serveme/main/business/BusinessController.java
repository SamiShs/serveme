package be.fkhu.serveme.main.business;

import be.fkhu.serveme.account.AccountController;
import be.fkhu.serveme.account.RegisterRequest;
import be.fkhu.serveme.account.user.User;
import be.fkhu.serveme.main.booking.Booking;
import be.fkhu.serveme.main.booking.BookingRequestHelper;
import be.fkhu.serveme.main.menu_item.MenuItem;
import be.fkhu.serveme.main.menu_item.MenuItemService;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Set;

@Controller
@SessionAttributes({"business", "cities"})
public class BusinessController {
    private final String VIEW_SEARCH_RESTAURANT = "searchRestaurant";
    private final String VIEW_RESTAURANT = "businesses";

    private final BusinessService businessService;
    private final MenuItemService menuItemService;

    public BusinessController(BusinessService businessService, MenuItemService menuItemService) {
        this.businessService = businessService;
        this.menuItemService = menuItemService;
    }

    @RequestMapping({"businesses"})
    public String showBusinesses(Model model) {
        model.addAttribute("businesses", businessService.findAllBusinesses());
        model.addAttribute("newBusiness", new Business());
        return "pages/businesses";
    }

    @PostMapping("createBusiness")
    public String saveOrUpdate(@ModelAttribute Business business) {
        Business savedBusiness = businessService.safeBusiness(business);
        return "redirect:/businesses";
    }
    @RequestMapping("manager/business/{id}")
    public String showBusinessByIdManager(@PathVariable String id, Model model, Business business, HttpSession session) {
        copyBusinessSafe(businessService.findBusinessById(Long.valueOf(id)), business);
        session.setAttribute("business", business);
        model.addAttribute("newMenuItem", new MenuItem());
        model.addAttribute("manage", true);

        return "pages/business";
    }

    @RequestMapping("/business/{id}")
    public String showBusinessById(@PathVariable String id, Model model, Business business, HttpSession session) {
        copyBusinessSafe(businessService.findBusinessById(Long.valueOf(id)), business);
        session.setAttribute("business", business);
        model.addAttribute("newMenuItem", new MenuItem());
        System.out.println(model);
        System.out.println("inside showiiiiing");
        return "pages/business";
    }

    @PostMapping("createMenuItem")
    public String saveOrUpdate(@ModelAttribute MenuItem menuItem) {
        MenuItem savedMenuItem = menuItemService.saveMenuItem(menuItem);
        return "redirect:/businesses";
    }

    @GetMapping(VIEW_SEARCH_RESTAURANT)
    public String searchRestaurant(Model model, SearchRequest searchRequest) {
        List<Business> businesses = businessService.searchBusiness(searchRequest);
        model.addAttribute("businesses", businesses);
        model.addAttribute("searchRequest", searchRequest);
        return VIEW_SEARCH_RESTAURANT;
    }


    private void copyBusinessSafe(Business businessDB, Business business) {
        business.setName(businessDB.getName());
        business.setDescription(businessDB.getDescription());
        business.setId(businessDB.getId());
        business.setMenuItems(businessDB.getMenuItems());
    }

    @ModelAttribute("business")
    public Business getBusiness() {
        Business business = new Business();
        business.setId(-1l);
        return business;
    }
    @ModelAttribute("cities")
    public String[] getCities() {
        return AccountController.vlaamseSteden;
    }
}