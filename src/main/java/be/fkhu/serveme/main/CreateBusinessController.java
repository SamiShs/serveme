package be.fkhu.serveme.main;

import be.fkhu.serveme.main.address.Address;
import be.fkhu.serveme.main.business.Business;
import be.fkhu.serveme.main.business.BusinessService;
import be.fkhu.serveme.main.menu_item.MenuItem;
import be.fkhu.serveme.main.menu_item.dto.MenuItemsCommand;
import be.fkhu.serveme.main.menu_item.dto.TablesCommand;
import be.fkhu.serveme.main.tables.Table;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Set;

@Controller
@SessionAttributes({
        "tempName",
        "tempDescription",
        "timeSlot",
        "tempAddress",
        "tempTableSet",
        "tempMenuItemSet"})
public class CreateBusinessController {

    private final String HOOK_VIEW = "managerView/createBusiness";

    private final BusinessService service;
    Business business;

    public CreateBusinessController(BusinessService service) {
//        business = new Business();
        this.service = service;
//        business = service.findBusinessById(1L);
        System.out.println(business);
    }


    {
        business = new Business();
        business.setName("name");
        business.setDescription("description");

        Address address = new Address();
        address.setCity("city");
        address.setHouseNumber("number");
        address.setPostalCode("postal code");
        address.setStreet("street");
        business.setAddress(address);

//        for (int i = 0; i < 3; i++) {
//            MenuItem menuItem = new MenuItem();
//            menuItem.setId((long) i);
//            menuItem.setName("Pizza " + i);
//            menuItem.setPrice(9.99);
//            menuItem.setDescription("Yummy Pizza " + i);
//            business.getMenuItems().add(menuItem);
//        }
//
//        for (int i = 0; i < 5; i++) {
//            Table table = new Table();
//            //table.setId((long) i);
//            table.setNumberOfPlaces(5);
//            //table.setTableNumber(i);
//            business.getTables().add(table);
//        }
    }

    //SESSION
    @ModelAttribute("tempName")
    private String setNameAttribute() {
        return business.getName();
    }

    @ModelAttribute("tempDescription")
    private String setDescriptionAttribute() {
        return business.getDescription();
    }
    @ModelAttribute("timeSlot")
    private int setTimeSlotAttribute() {
        return business.getTimeSlot();
    }

    @ModelAttribute("tempAddress")
    private Address setAddressAttribute() {
        return business.getAddress();
    }

    @ModelAttribute("tempMenuItemSet")
    private MenuItemsCommand setMenuItemsCommand() {
        return new MenuItemsCommand(business.getMenuItems());
    }

    @ModelAttribute("tempTableSet")
    private TablesCommand setTablesCommand() {
        return new TablesCommand(business.getTables());
    }

    @ModelAttribute("newMenuItem")
    private MenuItem setNewMenuItem() {
        return new MenuItem();
    }

    @ModelAttribute("newTable")
    private Table setNewTable() {
        return new Table();
    }

    //MAIN
    @GetMapping("/create")
    public String goToCreateBusinessPage() {
        return HOOK_VIEW;
    }

    //REQUESTS
    @PostMapping("/editName")
    public String editName(
            @RequestParam("name") String name,
            Model model) {
        model.addAttribute("tempName", name);
        return HOOK_VIEW;
    }

    @PostMapping("/editDescription")
    public String editDescription(
            @RequestParam("description") String description,
            Model model) {
        model.addAttribute("tempDescription", description);
        return HOOK_VIEW;
    }

    @PostMapping("/editTimeSlot")
    public String editTimeSlot(
            @RequestParam("timeSlot") int timeSlot,
            Model model) {
        model.addAttribute("timeSlot", timeSlot);
        return HOOK_VIEW;
    }


    @PostMapping("/editAddress")
    public String editAddress(@ModelAttribute("tempAddress") Address address) {
        return HOOK_VIEW;
    }

    //MENU-ITEMS
    @PostMapping("menuItem/create")
    public String create(
            @ModelAttribute("newMenuItem") MenuItem menuItem,
            @ModelAttribute("tempMenuItemSet") MenuItemsCommand menuItems) {
        menuItems.addMenuItem(menuItem);
        return HOOK_VIEW;
    }

    @PostMapping("menuItem/update")
    public String update(
            @ModelAttribute("tempMenuItemSet") MenuItemsCommand menuItems) {
        return HOOK_VIEW;
    }

    @GetMapping("menuItem/{id}/delete")
    public String deleteById(
            @ModelAttribute("tempMenuItemSet") MenuItemsCommand menuItems,
            @PathVariable String id) {
        MenuItem menuItem = menuItems.getMenuItems()
                .stream()
                .filter(n -> n.getId() == Long.parseLong(id))
                .findAny().orElse(null);
        if (Objects.nonNull(menuItem))
            menuItems.getMenuItems().remove(menuItem);
        return HOOK_VIEW;
    }

    //TABLES
    @PostMapping("table/create")
    public String createTable(
            @ModelAttribute("newTable") Table table,
            @ModelAttribute("tempTableSet") TablesCommand tables) {
        tables.addTable(table);
        return HOOK_VIEW;
    }

    @PostMapping("table/update")
    public String updateTable(
            @ModelAttribute("tempTableSet") TablesCommand tables) {
        return HOOK_VIEW;
    }

    @GetMapping("table/{id}/delete")
    public String deleteTableById(
            @ModelAttribute("tempTableSet") TablesCommand tables,
            @PathVariable String id) {
        Table table = tables.getTables()
                .stream()
                .filter(n -> n.getId() == Long.parseLong(id))
                .findAny().orElse(null);
        if (Objects.nonNull(table))
            tables.getTables().remove(table);
        return HOOK_VIEW;
    }

    @GetMapping("create/Business")
    public String createBusiness(
            @ModelAttribute("tempName") String name,
            @ModelAttribute("tempDescription") String description,
            @ModelAttribute("tempAddress") Address address,
            @ModelAttribute("tempMenuItemSet") MenuItemsCommand menuItemsCommand,
            @ModelAttribute("tempTableSet") TablesCommand tablesCommand
    ) {
        List<MenuItem> menuItems = menuItemsCommand.getMenuItems();
        Set<Table> tables = tablesCommand.getTables();

//        Business business = new Business();
        business.setName(name);

//        address.setAddress_ID(null);
        business.setAddress(address);
        business.setDescription(description);


        for (MenuItem menuItem : menuItems) {
            menuItem.setId(null);
            business.addMenuItem(menuItem);
        }

        for (Table table : tables) {
            business.addTable(table);
        }

//        System.out.println("---------------------------------- repo");
//        System.out.println(service.findBusinessById(1L));
//        System.out.println("---------------------------------- edit");
//        business.setId(1L);


        service.safeBusiness(business);

        return HOOK_VIEW;
    }
}