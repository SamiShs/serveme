package be.fkhu.serveme.main;

import be.fkhu.serveme.main.business.Business;
import be.fkhu.serveme.main.category.Category;
import be.fkhu.serveme.main.menu_item.MenuItem;
import be.fkhu.serveme.main.order.Order;
import be.fkhu.serveme.main.order_menuItem.OrderMenuItem;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@SessionAttributes({"currentOrderedItemList"})
public class CustomerOrderController {

    private final CustomerOrderService customerOrderService;
    private long sessionId=0;


    public CustomerOrderController(CustomerOrderService customerOrderService) {
        this.customerOrderService = customerOrderService;
    }

    @ModelAttribute("menuItems")
    private List<MenuItem> showMenuItems(){
        return customerOrderService.findAllMenuItems();
    }

    @ModelAttribute("categories")
    private Set<Category> businessCategories(HttpSession session){
        Business business = (Business) session.getAttribute("business");
        return customerOrderService.findAllCategoriesForBusiness(business.getId());
    }

    @RequestMapping("/menuItems")
    public String showMenuItemList(Model model){
        model.addAttribute("currentOrderedItemList", new HashSet<>());
        return "ordering/menuItems";
    }

    @RequestMapping("/menuItemsBack")
    public String showMenuItemListBack(){
        return "ordering/menuItems";
    }


    @GetMapping("/showMenu")
    public String showMenuForRestaurant(HttpSession session, Model model) {
        model.addAttribute("currentOrderedItemList", new HashSet<>());
        Business business = (Business) session.getAttribute("business");
        model.addAttribute("currentOrderedItemList", new HashSet<>());
        model.addAttribute("menuItems", business.getMenuItems());
        return "ordering/menuItems";
    }

    @RequestMapping("menuItem/{id}/show")
    public String showMenuItemInfo(@PathVariable String id, Model model) {
        model.addAttribute("orderMenuItem", new OrderMenuItem());
        model.addAttribute("menuItem", customerOrderService.findMenuItemById(id));
        return "ordering/menuItem";
    }

    @ModelAttribute("currentOrderedItemList")
    private Set<OrderMenuItem> addCurrentOrderedItemListToSession(HttpSession session)
    {
        Set<OrderMenuItem> set = new HashSet<>();
        session.setAttribute("currentOrderedItemList", set);
        return new HashSet<>();
    }

    @GetMapping("/addMenuItem")
    public String addOrderMenuItemToOrderList(
            @Valid OrderMenuItem orderMenuItem,
            BindingResult bindingResult,
            @RequestParam(value = "id") String id,
            @ModelAttribute("currentOrderedItemList") Set<OrderMenuItem> list
    ){
        //MenuItem menuItem = findMenuItemById(menuItemId);
        //OrderMenuItem orderMenuItem =
        //  new OrderMenuItem( menuItem, Integer.parseInt(amount), currentOrder,currentOrder.getOrderMenuItems().size() + 1);
        //currentOrder.addOrderMenuItemToSet(orderMenuItem);

        if (bindingResult.hasErrors()) {
            return "ordering/menuItems";
        }else {

            orderMenuItem.setMenuItem(customerOrderService.findMenuItemById(id));
            orderMenuItem.setSessionId(++sessionId);
            list.add(orderMenuItem);
            return "ordering/menuItems";
        }
   }
    @GetMapping("/viewBasket")
    public String viewBasketWithList(@ModelAttribute("currentOrderedItemList") Set<OrderMenuItem> orderMenuItemSet, Model model,HttpSession session){
        double price =0;
        for (OrderMenuItem orderMenuItem : orderMenuItemSet) {
            price += orderMenuItem.getTotalPrice();
        }
        model.addAttribute("price", price);
        session.setAttribute("price", price + "");
        return "ordering/basket";
    }

    @GetMapping("/pay")
    public String paySet(HttpSession session,Model model) {
        String price = (String) session.getAttribute("price");
        try {
            Business business = (Business) session.getAttribute("business");
            System.out.println("price + "+price);
            String[] urls = customerOrderService.createPaymentRequest(price, business.getName(), business.getBookings().size() + 1 + "");
            session.setAttribute("checkPayment", urls[1]);
            return "redirect:" + urls[0];
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("error payment creation");
            model.addAttribute("paymentError", "payment service failed, please try again later");
            return "ordering/basket";
        }
    }

    @GetMapping("paymentCheck")
    private String paymentCheck(HttpSession session,@ModelAttribute("currentOrderedItemList") Set<OrderMenuItem> orderMenuItemSet,Model model) {
        String checkURL = (String) session.getAttribute("checkPayment");
        try {
            String status = customerOrderService.checkPaymentStatus(checkURL);
            if (status.equals("paid")) {
                Order order = new Order();
                order.addOrderMenuItemSet(orderMenuItemSet);
                order.setTime(LocalDateTime.now());
                customerOrderService.saveCurrentOrder(order);
                sessionId = 0;
                model.addAttribute("currentOrderedItemList", new HashSet<>());
                System.out.println("payment is passed");
                model.addAttribute("messageSuccess","payment for Order finished successful");
                return "userPanel/user";
            }else {
                System.out.println("payment failed");
                model.addAttribute("paymentError", "payment failed, please try payin again");
                return viewBasketWithList(orderMenuItemSet,model,session);
            }
        } catch (Exception e) {
            System.out.println("checking payment errrrror");
            model.addAttribute("paymentError", "payment failed, please try payin again");
            return "ordering/basket";
        }

    }


    @GetMapping("removeItemFromOrder")
    public String removeItemFromOrder(
            @ModelAttribute("currentOrderedItemList") Set<OrderMenuItem> orderMenuItemSet,
            @RequestParam("sessionId") String sessionID){
        Long id = Long.parseLong(sessionID);
        orderMenuItemSet.removeIf(orderMenuItem -> orderMenuItem.getSessionId().equals(id));
        return "ordering/basket";
    }


    @GetMapping("/showCategory")
    public String returnCategoryResultsList(@RequestParam String categoryName, Model model, HttpSession session){
        Business business = (Business)session.getAttribute("business");
        List<MenuItem> resultsListOfSearchByCategory =customerOrderService.returnMenuItemsByCategory(categoryName, business.getId());
        model.addAttribute("resultList", resultsListOfSearchByCategory);
        return "ordering/category";
    }



//TODO

//    @ModelAttribute("currentOrder")
//    private Order addCurrentOrderToSession(){
//        return customerOrderService.createNewOrder();
//    }
//    @GetMapping("/addMenuItem")
//    public String addMenuItemToBasket(
//            @RequestParam(value = "amount", required = false, defaultValue = "0") String amount,
//            @RequestParam(value = "id") String id,
//            @ModelAttribute("currentOrder") Order order){
//
//        customerOrderService.saveChosenMenuItemToCurrentOrder(id, amount, order,++sessionId);
//        return "ordering/menuItems";
//    }
//    @GetMapping("/viewBasket")
//    public String viewBasket(@ModelAttribute("currentOrder") Order order){
//        return "ordering/basket";
//    }
//    @GetMapping("/pay")
//    public String pay(@ModelAttribute("currentOrder") Order order, Model model){
//        customerOrderService.saveCurrentOrder(order);
//        model.addAttribute("currentOrder", new Order());
//        return "ordering/menuItems";
//    }
//    @GetMapping("removeItemFromOrder")
//    public String removeItemFromOrder(
//            @ModelAttribute("currentOrder") Order currentOrder,
//            @RequestParam("sessionId") String sessionID){
//        Long id = Long.parseLong(sessionID);
//        currentOrder.getOrderMenuItems().removeIf(orderMenuItem -> orderMenuItem.getSessionId().equals(id));
//        return "ordering/basket";
//    }
}


//    //1.Get All Business Categories

//      2.Make buttons for each category
//      //businessCategories        //On button click > filter by category
//      //@GetMapping("/showCategory)
