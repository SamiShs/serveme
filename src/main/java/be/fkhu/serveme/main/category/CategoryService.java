package be.fkhu.serveme.main.category;

import java.util.List;

public interface CategoryService {
    Category saveCategory(Category category);
    Category findCategoryByName(String description);
    List<Category> findALl();
}
