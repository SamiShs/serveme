package be.fkhu.serveme.main.category;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService{
    private final CategoryRepository repository;

    public CategoryServiceImpl(CategoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public Category saveCategory(Category category) {
        Category savedCategory = repository.save(category);
        return savedCategory;
    }

    @Override
    public Category findCategoryByName(String name) {
        Optional<Category> categoryOptional = repository.findByName(name);
        if(categoryOptional.isEmpty()){
            throw new RuntimeException("Category Not Found");
        }
        return categoryOptional.get();
    }

    @Override
    public List<Category> findALl() {
        return repository.findAll();
    }
}
