package be.fkhu.serveme.main.category;

import be.fkhu.serveme.main.menu_item.MenuItem;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import static java.util.Comparator.comparing;

@Entity
@Table(name = "Category")
public class Category {

    @Transient
    public static final Comparator<Category> BY_NAME = comparing(Category::getName);

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_id")
    private Long id;

    @Column(name = "category_name")
    private String name;

    @ManyToMany(mappedBy = "categories")
    private Set<MenuItem> menuItems = new HashSet<>();

    public Category(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(Set<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Category{");
        sb.append("name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
