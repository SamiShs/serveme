package be.fkhu.serveme.main.booking;


import be.fkhu.serveme.account.user.User;
import be.fkhu.serveme.account.user.UserRepository;
import be.fkhu.serveme.exceptions.NoTableAvailableException;
import be.fkhu.serveme.exceptions.TableSelectionException;
import be.fkhu.serveme.main.business.Business;
import be.fkhu.serveme.main.business.BusinessRepository;
import be.fkhu.serveme.main.tables.Table;
import be.fkhu.serveme.main.tables.TableRepository;
import org.springframework.stereotype.Service;

import java.net.UnknownServiceException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Service
public class BookingServiceImpl implements BookingService {

    private final BookingRepository bookingRepository;
    private final UserRepository userRepository;
    private final BusinessRepository businessRepository;
    private final TableRepository tableRepository;


    public BookingServiceImpl(BookingRepository bookingRepository, UserRepository userRepository, BusinessRepository businessRepository, TableRepository tableRepository) {
        this.bookingRepository = bookingRepository;
        this.userRepository = userRepository;
        this.businessRepository = businessRepository;
        this.tableRepository = tableRepository;
    }

    @Override
    public Booking saveBooking(BookingRequestHelper bookingRequestHelper) {
        Table table = tableRepository.getOne(Long.parseLong(bookingRequestHelper.getTableID()));
        int amountOfPersons = Integer.parseInt(bookingRequestHelper.getNumberCustomers());
        if (table.getNumberOfPlaces() < amountOfPersons || amountOfPersons <= 0) {
            throw new TableSelectionException("please book separate tables if one table is not enough");
        }
        User user = userRepository.getOne(Long.parseLong(bookingRequestHelper.getUserID()));
        Business business = businessRepository.getOne(Long.parseLong(bookingRequestHelper.getRestaurantID()));
        Booking booking = new Booking();
        booking.setBusiness(business);
        booking.setUser(user);
        booking.setAmountOfPersons(Integer.parseInt(bookingRequestHelper.getNumberCustomers()));
        //2011-12-03T10:15:30'
        LocalDateTime localDateTime = LocalDateTime.parse(bookingRequestHelper.getDateTimeString()
                , DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        booking.setTimeAtRestaurant(localDateTime);
        booking.addTable(table);
        System.out.println("saving this booking now" + booking);
        bookingRepository.save(booking);
        System.out.println("after repo -------------");
        return booking;
    }

    @Override
    public void updateBooking(Booking booking) {
        Booking bookingToBeUpdated = bookingRepository.getReservationById(booking.getId());
        bookingToBeUpdated.setTables( booking.getTables());
        bookingToBeUpdated.setAmountOfPersons(booking.getAmountOfPersons());
        bookingToBeUpdated.setTimeAtRestaurant(booking.getTimeAtRestaurant());
        bookingRepository.save(bookingToBeUpdated);
    }
    @Override
    public Set<Booking> getAllBookingsForUser(long id) {
        User user = userRepository.getOne(id);
        return user.getUserBookings();

    }

    @Override
    public List<Table> getAvailableTablesForRequest(BookingRequestHelper bookingRequestHelper) {
        System.out.println("printing the request at bookingservice"+bookingRequestHelper);
        Business business = businessRepository.getOne(Long.parseLong(bookingRequestHelper.getRestaurantID()));
        List<Table> tables = new ArrayList(business.getTables());
        LocalDateTime askedTimeSlot = LocalDateTime.parse(bookingRequestHelper.getDateTimeString(), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
                tables.removeIf(table ->
                {
                    for (Booking booking : table.getBookings()) {
                        return Math.abs(Duration.between(booking.getTimeAtRestaurant(), askedTimeSlot).toMinutes())<business.getTimeSlot();
                    }
                    return false;
                });
        if (tables.isEmpty()) {
            throw new NoTableAvailableException("No tables available at selected time");
        }
        tables.sort(Comparator.comparing(Table::getNumberOfPlaces));
        for (int i = 0; i < tables.size()-1;) {
            if (tables.get(i).getNumberOfPlaces() == tables.get(i + 1).getNumberOfPlaces()) {
                tables.remove(i);
            }else {
                i++;
            }
        }
        return tables;
    }
}