package be.fkhu.serveme.main.booking;

import be.fkhu.serveme.account.user.User;
import be.fkhu.serveme.exceptions.NoTableAvailableException;
import be.fkhu.serveme.exceptions.TableSelectionException;
import be.fkhu.serveme.main.business.Business;
import be.fkhu.serveme.main.tables.Table;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Controller
@SessionAttributes({"user", "business"})
public class BookingController {
    private final String BOOKING = "booking/";
    private final String VIEW_BOOKING = "bookings";
    private final String VIEW_NEWBOOKING = "addNewBooking";
    private final String VIEW_LOGIN = "login";
    private final String REDIRECT = "redirect:";

    private final BookingService bookingService;

    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    /**
     * Show bookings.
     *
     * @param model the model
     * @return the string
     */
    @GetMapping(VIEW_BOOKING)
    public String showBooking(Model model, User user) {
        Set<Booking> bookings;
        bookings = bookingService.getAllBookingsForUser(user.getUser_ID());
        model.addAttribute("userBookings", bookings);
        return BOOKING + VIEW_BOOKING;
    }

    /**
     * New booking request.
     *
     * @return the string
     */
    @GetMapping(VIEW_NEWBOOKING)
    public String NewBooking(BookingRequestHelper bookingRequestHelper, Business business, User user,
                             ArrayList<Table> tableList) {
        System.out.println("inside get method of new booking "+ tableList );
        if (business.getId()==-1l) {
            return "redirect:businesses";
        }
        bookingRequestHelper.setUserID(user.getUser_ID() + "");
        bookingRequestHelper.setRestaurantID(business.getId() + "");
        bookingRequestHelper.setRestaurantName(business.getName());
        return BOOKING + VIEW_NEWBOOKING;
    }

    /**
     * Add new booking request.
     *
     * @param bookingRequestHelper the booking request helper
     * @return the string
     */
    @PostMapping("post" + VIEW_NEWBOOKING)
    public String addNewBooking(BookingRequestHelper bookingRequestHelper, Business business, HttpSession session, Model model) {
        if (bookingRequestHelper.getUserID().equals("-1")) {
            session.setAttribute("messageToLogin", "please login before you can make reservation");
            session.setAttribute("redirectURL", VIEW_NEWBOOKING);
            return REDIRECT + VIEW_LOGIN;
        }
        try {
            if (Objects.isNull(bookingRequestHelper.getTableID())) {

                bookingRequestHelper.setButtonChanger("save Booking");
                model.addAttribute("tableList", bookingService.getAvailableTablesForRequest(bookingRequestHelper));
                model.addAttribute("BookingRequestHelper", bookingRequestHelper);
                return BOOKING + VIEW_NEWBOOKING;
            }
            bookingService.saveBooking(bookingRequestHelper);
        } catch (TableSelectionException tse) {
            bookingRequestHelper.setButtonChanger("save Booking");
            model.addAttribute("tableList", bookingService.getAvailableTablesForRequest(bookingRequestHelper));
            model.addAttribute("BookingRequestHelper", bookingRequestHelper);
            model.addAttribute("tableSelectionError", tse.getMessage());
            return BOOKING + VIEW_NEWBOOKING;

        } catch (NoTableAvailableException ntae) {
            model.addAttribute("BookingRequestHelper", bookingRequestHelper);
            model.addAttribute("tableSelectionError", ntae.getMessage());
            return BOOKING + VIEW_NEWBOOKING;

        }catch (Exception ex){
            ex.printStackTrace();
            return REDIRECT+VIEW_LOGIN;

        }

        business.setId(-1l);
        return REDIRECT + VIEW_BOOKING;
    }

    @ModelAttribute("user")
    public User getUser(HttpSession session) {
        User user = (User) session.getAttribute("user");
        return user;
    }

    @ModelAttribute("business")
    public Business getBusiness(HttpSession session) {
        Business business = (Business) session.getAttribute("business");
        return business;
    }

}
