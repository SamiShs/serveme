package be.fkhu.serveme.main.booking;

import be.fkhu.serveme.account.user.User;
import be.fkhu.serveme.main.business.Business;
import be.fkhu.serveme.main.tables.Table;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public interface BookingService {
    Booking saveBooking(BookingRequestHelper bookingRequestHelper);
    public void updateBooking(Booking booking);

    Set<Booking> getAllBookingsForUser(long id);

    List<Table> getAvailableTablesForRequest(BookingRequestHelper bookingRequestHelper);
}
