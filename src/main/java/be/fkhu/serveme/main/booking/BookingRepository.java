package be.fkhu.serveme.main.booking;

import be.fkhu.serveme.account.user.User;

import be.fkhu.serveme.main.business.Business;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Integer> {
    public List<Booking> getAllBookingsByUser(User user);
    public List<Booking> getAllBookingsByBusiness(Business business);
    default void saveReservation(Booking booking){
        save(booking);
    }
    Booking getReservationById(long id);


}
