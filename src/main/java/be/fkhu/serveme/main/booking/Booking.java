package be.fkhu.serveme.main.booking;

import be.fkhu.serveme.account.user.User;

import be.fkhu.serveme.main.menu_item.MenuItem;
import be.fkhu.serveme.main.tables.Table;
import be.fkhu.serveme.main.business.Business;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@javax.persistence.Table(name = "Booking")
public class Booking implements Serializable {

    //Fields
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Booking_ID")
    private long id;
    @ManyToOne
    @JoinColumn(name = "User_ID", referencedColumnName = "User_ID")
    private User user;
    @Column(name = "Number_Of_Customers")
    private int amountOfPersons;
    @Column(name="DateTime")
    private LocalDateTime timeAtRestaurant;
    @ManyToOne
    @JoinColumn(name="Business_ID", referencedColumnName = "Business_ID")
    private Business business;
    @ManyToMany(mappedBy = "bookings", cascade = CascadeType.PERSIST)
    private Set<Table> tables = new HashSet<>();


    //Constructors
    public Booking() {
    }


    public Booking(User user, int amountOfPersons, LocalDateTime timeAtRestaurant, Business business) {
        this.user = user;
        this.amountOfPersons = amountOfPersons;
        this.timeAtRestaurant = timeAtRestaurant;
        this.business = business;
    }

    //Getters & Setters
    public User getUser() {
        return user;
    }

    public void setUser(User customer) {
        this.user = customer;
    }

    public int getAmountOfPersons() {
        return amountOfPersons;
    }

    public void setAmountOfPersons(int amountOfPersons) {
        this.amountOfPersons = amountOfPersons;
    }

    public LocalDateTime getTimeAtRestaurant() {
        return timeAtRestaurant;
    }

    public void setTimeAtRestaurant(LocalDateTime timeAtRestaurant) {
        this.timeAtRestaurant = timeAtRestaurant;
    }

    protected long getId() {
        return id;
    }

    private void setId(long id) {
        this.id = id;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public Set<Table> getTables() {
        return tables;
    }

    public void setTables(Set<Table> tables) {
        this.tables = tables;
    }




    //Methodes
    public void addTable(Table table){
        tables.add(table);
        table.addBooking(this);
    }



    //equals, hashcode & toString


//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        Booking that = (Booking) o;
//
//        if (id != that.id) return false;
//        if (amountOfPersons != that.amountOfPersons) return false;
//        if (user != null ? !user.equals(that.user) : that.user != null) return false;
//        if (timeAtRestaurant != null ? !timeAtRestaurant.equals(that.timeAtRestaurant) : that.timeAtRestaurant != null)
//            return false;
//        if (business != null ? !business.equals(that.business) : that.business != null) return false;
//        return tables != null ? tables.equals(that.tables) : that.tables == null;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = (int) (id ^ (id >>> 32));
//        result = 31 * result + (user != null ? user.hashCode() : 0);
//        result = 31 * result + amountOfPersons;
//        result = 31 * result + (timeAtRestaurant != null ? timeAtRestaurant.hashCode() : 0);
//        result = 31 * result + (business != null ? business.hashCode() : 0);
//        result = 31 * result + (tables != null ? tables.hashCode() : 0);
//        return result;
//    }


    @Override
    public String toString() {
        return "Booking{" +
                "user=" + user +
                ", amountOfPersons=" + amountOfPersons +
                ", timeAtRestaurant=" + timeAtRestaurant +
                ", business=" + business +
                ", tables=" + tables +
                '}';
    }
}
