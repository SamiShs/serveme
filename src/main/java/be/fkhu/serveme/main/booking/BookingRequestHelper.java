package be.fkhu.serveme.main.booking;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class BookingRequestHelper {

    private String userID;
    private String restaurantID;
    private String numberCustomers;
    private String dateTimeString;
    private String dateTimeStringMin;
    private String dateTimeStringMax;
    private String restaurantName;
    private String tableSize;
    private String tableID;
    private String buttonChanger = "select table";

    public void setNumberCustomers(String numberCustomers) {
        this.numberCustomers = numberCustomers;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setRestaurantID(String restaurantID) {
        this.restaurantID = restaurantID;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getUserID() {
        return userID;
    }

    public String getRestaurantID() {
        return restaurantID;
    }

    public String getNumberCustomers() {
        return numberCustomers;
    }

    public void setDateTimeString(String dateTimeString) {
        this.dateTimeString = dateTimeString;
    }

    public String getDateTimeString() {
        return dateTimeString;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public String getDateTimeStringMin() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm"));
    }

    public String getDateTimeStringMax() {
        return LocalDateTime.now().plusMonths(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm"));
    }

    public String getButtonChanger() {
        return buttonChanger;
    }

    public void setButtonChanger(String buttonChanger) {
        this.buttonChanger = buttonChanger;
    }

    public String getTableSize() {
        return tableSize;
    }

    public void setTableSize(String tableSize) {
        this.tableSize = tableSize;
    }

    public String getTableID() {
        return tableID;
    }

    public void setTableID(String tableID) {
        this.tableID = tableID;
    }

    @Override
    public String toString() {
        return "BookingRequestHelper{" +
                "userID='" + userID + '\'' +
                ", restaurantID='" + restaurantID + '\'' +
                ", numberCustomers='" + numberCustomers + '\'' +
                ", dateTimeString='" + dateTimeString + '\'' +
                ", restaurantName='" + restaurantName + '\'' +
                ", tableSize='" + tableSize + '\'' +
                ", tableID='" + tableID + '\'' +
                ", buttonChanger='" + buttonChanger + '\'' +
                '}';
    }
}
