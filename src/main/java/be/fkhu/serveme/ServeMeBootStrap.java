package be.fkhu.serveme;
import be.fkhu.serveme.account.login.Login;
import be.fkhu.serveme.account.user.Role;
import be.fkhu.serveme.account.user.User;
import be.fkhu.serveme.account.user.UserRepository;
import be.fkhu.serveme.main.address.Address;
import be.fkhu.serveme.main.booking.BookingService;
import be.fkhu.serveme.main.business.Business;
import be.fkhu.serveme.main.business.BusinessService;
import be.fkhu.serveme.main.category.Category;
import be.fkhu.serveme.main.category.CategoryService;
import be.fkhu.serveme.main.menu_item.MenuItem;
import be.fkhu.serveme.main.menu_item.MenuItemRepository;
import be.fkhu.serveme.main.menu_item.MenuItemService;
import be.fkhu.serveme.main.order.OrderService;
import be.fkhu.serveme.main.tables.Table;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
@Component
public class ServeMeBootStrap implements ApplicationListener<ContextRefreshedEvent> {
    private final MenuItemService menuItemService;
    private final CategoryService categoryService;
    private final BusinessService businessService;
    private final OrderService orderService;
    private final BookingService bookingService;
    private final UserRepository userRepository;
    private final MenuItemRepository repository;
    public ServeMeBootStrap(MenuItemService menuItemService, CategoryService categoryService, BusinessService businessService, OrderService orderService, BookingService bookingService, UserRepository userRepository, MenuItemRepository repository) {
        this.menuItemService = menuItemService;
        this.categoryService = categoryService;
        this.businessService = businessService;
        this.orderService = orderService;
        this.bookingService = bookingService;
        this.userRepository = userRepository;
        this.repository = repository;
    }
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
//        populate();
//
//        Table table1 = new Table();
//        table1.setNumberOfPlaces(5);
//
//
//        //Get All Business Categories
//        Set<MenuItem> menuItems = businessService.findBusinessById(1L).getMenuItems();
//        Set<Category> businessCategories = new HashSet<>();
//        for (MenuItem menuItem : menuItems) {
//            businessCategories.addAll(menuItem.getCategories());
//        }
//        businessCategories.forEach(System.out::println);
//
//
//        //Method filter by category
//        menuItems.stream().filter(menuItem -> {
//            for (Category category : menuItem.getCategories()) {
//                if(category.getName().equals("Vegan")) return true;
//            }
//            return false;
//        }).forEach(System.out::println);
    }
    void populate(){
        //--------------------------------------------------------      CATEGORIES
        //------------------------------------------------------------------------
        Category category1 = new Category();
        category1.setName("Vegan");
        Category category2 = new Category();
        category2.setName("Indian");
        Category category3 = new Category();
        category3.setName("Italian");
        categoryService.saveCategory(category1);
        categoryService.saveCategory(category2);
        categoryService.saveCategory(category3);
        //--------------------------------------------------------      MENU ITEMS
        //------------------------------------------------------------------------
        MenuItem item1 = new MenuItem();
        item1.setName("Buddha Bowl");
        item1.setPrice(9.5);
        item1.setDescription("Healthy, hearty, and hella good.");
        item1.getCategories().add(categoryService.findCategoryByName("Vegan"));
        item1.getCategories().add(categoryService.findCategoryByName("Indian"));
        MenuItem item2 = new MenuItem();
        item2.setName("Buddha Bowl EXTRA");
        item2.setPrice(9.5);
        item2.setDescription("Healthy, hearty, and hella hella hella good.");
        item2.getCategories().add(categoryService.findCategoryByName("Vegan"));
        item2.getCategories().add(categoryService.findCategoryByName("Indian"));
        MenuItem item3 = new MenuItem();
        item3.setName("Buddha Bowl EXTRA EXTRA EXTRA");
        item3.setPrice(9.5);
        item3.setDescription("Healthy, hearty, and hella hella hella hella hella hella hella good.");
        item3.getCategories().add(categoryService.findCategoryByName("Vegan"));
        item3.getCategories().add(categoryService.findCategoryByName("Indian"));
        MenuItem item4 = new MenuItem();
        item4.setName("Soup");
        item4.setPrice(9.5);
        item4.setDescription("Just soup.");
        item4.getCategories().add(categoryService.findCategoryByName("Vegan"));
        // Address toevoegen
        //------------------
        Address address = new Address();
        address.setStreet("Moorselestraat");
        address.setPostalCode("9000");
        address.setHouseNumber("79");
        address.setCity("Ghent");
        Address address2 = new Address();
        address2.setStreet("SamirTheHackerStraat");
        address2.setPostalCode("9111");
        address2.setHouseNumber("79");
        address2.setCity("Dubai");
        Address address3 = new Address();
        address3.setStreet("PhyllisCrazyCatWomanStreet");
        address3.setPostalCode("8000");
        address3.setHouseNumber("19");
        address3.setCity("Springfield");
        Address address4 = new Address();
        address4.setStreet("WouterBestCoderStreet");
        address4.setPostalCode("6969");
        address4.setHouseNumber("420");
        address4.setCity("Fuckin'");

        Business business1 = new Business();
        business1.setName("Pizza Hut");
        business1.setDescription("You get a Pizza! And you get a Pizza... EVERYBODY gets a pizza!");
        business1.addMenuItem(item1);
        business1.addMenuItem(item2);
        business1.setAddress(address);
//        business1.setTables(tables);
        Business business2 = new Business();
        business2.setName("Hof Van Cleve");
        business2.setDescription("We gaan hierna toch nog een frietje gaan eten, eh mannen!?");
        business2.addMenuItem(item3);
        business2.setAddress(address2);
        Business business3 = new Business();
        business3.setName("Street Vendor");
        business3.setDescription("Questionable, but I am hungry, so whatever...");
        business3.addMenuItem(item4);
        business3.setAddress(address3);
        //tables-----------------------------
        Table table1 = new Table();
        table1.setNumberOfPlaces(5);
        table1.setBusiness(business1);
        Table table2 = new Table();
        table2.setNumberOfPlaces(7);
        table2.setBusiness(business1);
        Table table3 = new Table();
        table3.setNumberOfPlaces(6);
        table3.setBusiness(business1);
//        Set<Table> tables =new HashSet<>();
//        tables.add(table1);
//        tables.add(table2);
//        tables.add(table3);
        //--------------------------------------------------------      BUSINESSES
        //------------------------------------------------------------------------
        Login login = new Login();
        login.setPassWord("$2a$10$ZCKTFCCjfjQSKKGZWjk8uOVDmtB/BNv.MGP.m/OT.YqeNy9lWEUMy");
        login.setEmail("lol@lol.lol");
        User user = new User();
        user.setFamilyName("Andriessen");
        user.setName("Samir");
        user.setRole(Role.ROLE_MANAGER);
        user.setAddress(address4);
        user.setLogin(login);
        businessService.safeBusiness(business1);
        businessService.safeBusiness(business2);
        businessService.safeBusiness(business3);
        userRepository.save(user);
        //------------------------------------------------------------      ORDERS
        //------------------------------------------------------------------------
//        Order order1 = new Order();
//        order1.setTime(LocalDateTime.now());
//        order1.getMenuItems().add(item1);
//        order1.getMenuItems().add(item2);
//        orderService.saveOrder(order1);




        //TODO ADD SEPARATOR FOR GENERIC CODE IN SAFE AREA TO PREVENT IT FROM CLOGGING UP SPACE AT SOME IMPORTANT CODE BLOCKS!!!!!
        //TODO PRESENT GENERIC CODE FOR REVIEW AND POSSIBLE IMPLEMENTATION
        //TODO TELL OTHERS IT'S NOT THEM, IT'S US...

        //    public boolean checkIfObjectAlreadyExistsByName(String name){
//        List<Object> objects =objectService.findAll();
//        for (Object object :objects) {
//            object.getName().equals(name);
//        } return false;
//    }
//
//    public boolean checkIfObject1AlreadyHasAnObject2ConnectedToIt(Object1 object1){
//        List<Object2> object2List = object2Service.findAllObject2();
//        for (Object2 objectInObjectList : object2List){
//            if (objectInObjectList.getObject1().equals(object1)){
//                System.out.println("This object1 already has a object2 assigned to it");
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public void saveManyRelationshipsObject(Object object){
//          if(checkIfObjectAlreadyExistsByName(object.getName()){
//               model.addAttribute("errorMessage", "You already are using that name, you evil philly!";
//              return "creationPage";
//              }
//          else { objectService.saveObject(object)
//          }
//    }
//
//    public void saveOneToOneRelationshipObject2(Object2 object2, Model model){
//      Object1 object1 = object2.getObject1();
//      if(checkIfObject1AlreadyHasAnObject2ConnectedToIt(Object1 object1){
//               model.addAttribute("errorMessage", "You tried to add an extra object2 to an existing object1, you naughty Matthias!";
//              return "creationPage";
//      } else {
//       object2repository.saveObject2(object2);
//       }
//    }
//
//
//
    }
}