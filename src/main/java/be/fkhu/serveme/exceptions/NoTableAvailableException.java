package be.fkhu.serveme.exceptions;

public class NoTableAvailableException extends RuntimeException {
    public NoTableAvailableException() {
        super();
    }

    public NoTableAvailableException(String message) {
        super(message);
    }
}
