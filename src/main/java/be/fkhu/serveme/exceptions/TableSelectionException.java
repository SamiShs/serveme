package be.fkhu.serveme.exceptions;

public class TableSelectionException extends RuntimeException {
    public TableSelectionException() {
        super();
    }

    public TableSelectionException(String message) {
        super(message);
    }
}
