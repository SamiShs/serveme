package be.fkhu.serveme.account;

import be.fkhu.serveme.main.address.Address;
import be.fkhu.serveme.main.address.AddressRepository;
import be.fkhu.serveme.account.login.Login;
import be.fkhu.serveme.account.login.LoginRepository;
import be.fkhu.serveme.account.user.User;
import be.fkhu.serveme.account.user.UserRepository;
import be.fkhu.serveme.exceptions.UserAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * The type Account service.
 *
 * @author Samirullah
 */
@Service
public class AccountService {
    private final UserRepository userRepository;
    private final LoginRepository loginRepository;
    private final AddressRepository addressRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;

    /**
     * Instantiates a new Account service.
     *
     * @param userRepository        the user repository
     * @param loginRepository       the login repository
     * @param addressRepository     the address repository
     * @param passwordEncoder       the password encoder
     * @param authenticationManager the authentication manager
     */
    @Autowired
    AccountService(UserRepository userRepository, LoginRepository loginRepository, AddressRepository addressRepository,
                   PasswordEncoder passwordEncoder, AuthenticationManager authenticationManager) {

        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.addressRepository = addressRepository;
        this.loginRepository = loginRepository;
        this.passwordEncoder = passwordEncoder;

    }

    /**
     * Register new user.
     * checks if user already exists
     * checks if address already exits
     * creates new login, address and user
     *
     * @param registerRequest the register request from the view
     * @return the user to the view
     */
    public void registerNewUser(RegisterRequest registerRequest) {

        Optional<Login> loginOptional = loginRepository.findLoginByEmail(registerRequest.getEmail());
        Login login = loginOptional.orElse(null);
        if (!Objects.isNull(login))
            throw new UserAlreadyExistsException("user " + registerRequest.getEmail() + "already exists!");


        login = new Login();
        login.setEmail(registerRequest.getEmail());
        login.setPassWord(passwordEncoder.encode(registerRequest.getPassword()));

        Address address = new Address();
        List<Address> listOfAlreadyExistingAddress = addressRepository.findAdressByStreetAndHouseNumberAndPostalCode(registerRequest.getStreet(),
                registerRequest.getNumber(),registerRequest.getPostalCode());
        if (listOfAlreadyExistingAddress.isEmpty()) {

            address.setStreet(registerRequest.getStreet());
            address.setHouseNumber(registerRequest.getNumber());
            address.setPostalCode(registerRequest.getPostalCode());
            address.setCity(registerRequest.getCity());

        }else
            address = listOfAlreadyExistingAddress.get(0);



        User user = new User();
        user.setName(registerRequest.getFirstName());
        user.setFamilyName(registerRequest.getLastName());
        user.setAddress(address);
        user.setLogin(login);
        user.setRole(registerRequest.getRole());

        userRepository.save(user);

    }


    /**
     * Log in user.
     * creates an authentication object for the login request from the view.
     * performs authentication through security context.
     * sets SPRING_SECURITY_CONTEXT parameter on the session object to authorize requests
     *
     * @param httpServletRequest the http servlet request
     * @param loginRequest       the login request
     * @return the user
     */
    public User logIn(HttpServletRequest httpServletRequest, LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));
        SecurityContext sc = SecurityContextHolder.getContext();
        sc.setAuthentication(authentication);
        HttpSession session = httpServletRequest.getSession(true);
        session.setAttribute("SPRING_SECURITY_CONTEXT", sc);
        Optional<Login> loginOptional = loginRepository.findLoginByEmail(loginRequest.getEmail());

        return loginOptional.orElse(null).getUser();

    }

    public boolean userAuthorized(SecurityContext sc) {

        if(Objects.isNull(sc)) return false;
        return sc.getAuthentication().isAuthenticated();
    }

    /**
     * Log out.
     */
    public void logOut() {

        SecurityContextHolder.getContext().getAuthentication().setAuthenticated(false);

    }
}
