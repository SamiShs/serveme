package be.fkhu.serveme.account.user;

import be.fkhu.serveme.account.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Samirullah & Mauro
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
