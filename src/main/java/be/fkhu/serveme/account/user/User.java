package be.fkhu.serveme.account.user;

import be.fkhu.serveme.main.address.Address;
import be.fkhu.serveme.account.login.Login;
import be.fkhu.serveme.main.bill.Bill;
import be.fkhu.serveme.main.booking.Booking;

//import be.fkhu.serveme.jonathan.Reservation;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Samirullah & Mauro
 */

@Entity
@Table(name = "User")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "User_ID")
    private long User_ID;
    @Column(name = "First_name")
    private String name;
    @Column(name = "Last_name")
    private String familyName;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "Address_ID", referencedColumnName = "Address_ID")
    private Address address;
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "Login_ID",referencedColumnName = "Login_ID")
    private Login login;
    @Column(name = "Role")
    @Enumerated(EnumType.STRING)
    private Role role ;

    @OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private Set<Booking> userBookings = new HashSet<>();

    //relatie met Bill
    //TODO getters setters addbill()
    @OneToMany(mappedBy = "user")
    private Set<Bill> bills = new HashSet<>();


    public long getUser_ID() {
        return User_ID;
    }

    public void setUser_ID(long user_ID) {
        User_ID = user_ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
        login.setUser(this);
    }

    public Set<Booking> getUserBookings() {
        return userBookings;
    }

    public void setUserBookings(Set<Booking> userBookings) {
        this.userBookings = userBookings;
    }

    public void addUserBooking(Booking booking){
        userBookings.add(booking);
    }


    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return name.equals(user.name) &&
                familyName.equals(user.familyName) &&
                role == user.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, familyName, role);
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", familyName='" + familyName + '\'' +
                ", role=" + role +
                '}';
    }

}
