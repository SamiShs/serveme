package be.fkhu.serveme.account;
import be.fkhu.serveme.account.user.Role;
import be.fkhu.serveme.account.user.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.SessionScope;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Objects;

/**
 * The type Account controller is a backend controller for User registration, login, logout.
 * this controller listens for any subpaths of auth/
 *
 * @author Samirullah
 */
@Controller
@SessionAttributes({"user"})
public class AccountController {
    private final String VIEW_HOME = "index";
    private final String ACCOUNT = "account/";
    private final String VIEW_LOGIN = "login";
    private final String VIEW_REGISTER = "register";
    private final String USER_PANEL = "userPanel/";
    private final String VIEW_USER = "user";
    private final String REDIRECT = "redirect:";
    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * The get request for login.
     * If user is already logged in it returns the user panel view.
     *
     * @return the login view
     */
    @GetMapping({VIEW_HOME, "/"})
    public String getIndex(HttpSession session) {
        session.setAttribute("redirectURL", VIEW_HOME);
        return VIEW_HOME;
    }

    @GetMapping(VIEW_LOGIN)
    public String getLogin(LoginRequest loginRequest) {
        return ACCOUNT + VIEW_LOGIN;
    }

    /**
     * The backend accountController is used for performing login
     * If no login found a message is passed  to the view
     *
     * @param httpServletRequest the http servlet request is used for setting the authentication
     * @param loginRequest       the login request
     * @param model              the model
     * @return the string is a redirect to the page from which login is called
     */
    @PostMapping(VIEW_LOGIN + "post")
    public String logInAccount(HttpServletRequest httpServletRequest, LoginRequest loginRequest,
                               Model model, User user,@SessionAttribute String redirectURL) {
        try {
            copyUserSafe(accountService.logIn(httpServletRequest, loginRequest), user);
            httpServletRequest.getSession().setAttribute("user", user);
            httpServletRequest.getSession().getAttributeNames().asIterator().forEachRemaining(s -> System.out.println(s));
            return "redirect:" + redirectURL;
        } catch (Exception e) {
            model.addAttribute("loginError", "user not found");
            return ACCOUNT + VIEW_LOGIN;
        }
    }

    /**
     * Gets register request.
     *
     * @param model the model
     * @return the register
     */
    @GetMapping(VIEW_REGISTER)
    public String getRegister(Model model, RegisterRequest registerRequest) {
        model.addAttribute("cities", vlaamseSteden);
        return ACCOUNT + VIEW_REGISTER;
    }

    @PostMapping(VIEW_REGISTER)
    public String createAccount(HttpServletRequest httpServletRequest, @Valid RegisterRequest registerRequest, BindingResult bindingResult, User user, Model model) {
        if (bindingResult.hasErrors()) {
            return ACCOUNT + VIEW_REGISTER;
        }
        try {
            accountService.registerNewUser(registerRequest);

            LoginRequest loginRequest = new LoginRequest();
            loginRequest.setEmail(registerRequest.getEmail());
            loginRequest.setPassword(registerRequest.getPassword());

            copyUserSafe(accountService.logIn(httpServletRequest, loginRequest), user);
            httpServletRequest.getSession().setAttribute("user", user);
            return REDIRECT + VIEW_LOGIN;
        } catch (Exception e) {
            model.addAttribute("messageToRegister", "user with this username already exists!");
            return REDIRECT + VIEW_REGISTER;
        }

    }

    @GetMapping(VIEW_USER)
    public String userPanel() {
        return USER_PANEL + VIEW_USER;
    }


    @GetMapping("/logout")
    public String logOutAccount(HttpServletRequest httpServletRequest) {
        try {
            httpServletRequest.logout();
            accountService.logOut();
        } catch (Exception e) {
            return VIEW_HOME;
        }
        return REDIRECT + VIEW_HOME;
    }

    @ModelAttribute("user")
    public User getUser() {
        User user = new User();
        user.setUser_ID(-1l);
        return user;
    }
    private void copyUserSafe(User logIn, User user) {
        user.setName(logIn.getName());
        user.setUser_ID(logIn.getUser_ID());
        user.setFamilyName(logIn.getFamilyName());
        user.setRole(logIn.getRole());
    }


    public static String[] vlaamseSteden = {"Aalst",
            "Aarschot",
            "Antwerpen",
            "Beringen",
            "Bilzen",
            "Blankenberge",
            "Borgloon",
            "Bree",
            "Brugge",
            "Damme",
            "Deinze",
            "Dendermonde",
            "Diest",
            "Diksmuide",
            "Dilsen-Stokkem",
            "Eeklo",
            "Geel",
            "Genk",
            "Gent",
            "Geraardsbergen",
            "Gistel",
            "Halen",
            "Halle",
            "Hamont-Achel",
            "Harelbeke",
            "Hasselt",
            "Herentals",
            "Herk-de-Stad",
            "Hoogstraten",
            "Ieper",
            "Izegem",
            "Kortrijk",
            "Landen",
            "Leuven",
            "Lier",
            "Lokeren",
            "Lommel",
            "Lo-Reninge",
            "Maaseik",
            "Mechelen",
            "Menen",
            "Mesen",
            "Mortsel",
            "Nieuwpoort",
            "Ninove",
            "Oostende",
            "Oudenaarde",
            "Oudenburg",
            "Peer",
            "Poperinge",
            "Roeselare",
            "Ronse",
            "Scherpenheuvel-Zichem",
            "Sint-Niklaas",
            "Sint-Truiden",
            "Tielt",
            "Tienen",
            "Tongeren",
            "Torhout",
            "Turnhout",
            "Veurne",
            "Vilvoorde",
            "Waregem",
            "Waregem",
            "Wervik",
            "Zottegem",
            "Zoutleeuw"};

}