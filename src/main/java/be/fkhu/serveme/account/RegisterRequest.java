package be.fkhu.serveme.account;


import be.fkhu.serveme.account.user.Role;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

/**
 *
 * @author Samirullah
 */
public class RegisterRequest {
    @Email(message = "Please enter a valid E-mail")
    private String email;
    @Pattern(message = "Please enter a valid Password", regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$")
    private String password;
    @Pattern(message = "Please enter a valid first name", regexp = "^[A-Za-z]+$")
    private String firstName;
    @Pattern(message = "Please enter a valid last name", regexp = "^[A-Za-z]+$")
    private String lastName;
    private Role fieldRole;
    private String street;
    @Pattern(message = "Please enter a valid street number", regexp = "^[0-9]*$")
    private String number;
    @Pattern(message = "Please enter a valid postal code", regexp = "^[0-9]*$")
    private String postalCode;
    @Pattern(message = "Please enter a valid city name", regexp = "^[A-Za-z]+$")
    private String city;

    public RegisterRequest() {
    }


    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFieldRole() {
        return fieldRole+"";
    }

    public Role getRole() {
        return fieldRole;
    }

    public String getStreet() {
        return street;
    }

    public String getNumber() {
        return number;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFieldRole(String fieldRole) {
        this.fieldRole = fieldRole.strip().toUpperCase().equals("MANAGER")? Role.ROLE_MANAGER:Role.ROLE_CUSTOMER;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "RegisterRequest{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", role='" + fieldRole + '\'' +
                ", street='" + street + '\'' +
                ", number='" + number + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
