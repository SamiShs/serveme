package be.fkhu.serveme.account.auth;

import be.fkhu.serveme.account.user.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

import static java.util.Collections.singletonList;


/**
 * The type User details extends the Spring UserDetails.
 *
 * @author Samirullah
 */
public class UserDetailsImpl implements UserDetails {

    private String password;
    private String email;
    private GrantedAuthority authority;

    /**
     * Instantiates a new User details.
     *
     * @param email    the email
     * @param password the password
     * @param role     the role
     */
    public UserDetailsImpl(String email, String password, Role role) {
        this.password = password;
        this.email = email;
        this.authority = new SimpleGrantedAuthority(role.toString());
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return singletonList(authority);
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
