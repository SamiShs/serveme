package be.fkhu.serveme.account.auth;

import be.fkhu.serveme.account.login.Login;
import be.fkhu.serveme.account.login.LoginRepository;
import be.fkhu.serveme.exceptions.AccountNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * The type User details service.
 *
 * @author Samirullah
 */
@Service
@Primary
public class UserDetailsServiceImpl implements UserDetailsService {

    private final LoginRepository loginRepository;

    /**
     * Instantiates a new User details service.
     *
     * @param loginRepository the login repository is used to load a specific user from the database
     */
    @Autowired
    public UserDetailsServiceImpl(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    /**
     *
     * @param email is used to locate user in database
     * @return User details for spring to compare the login form input against
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<Login> loginOptional = loginRepository.findLoginByEmail(email);
        Login login = loginOptional.orElseThrow(() -> new AccountNotFoundException("no account found for " + email + "!"));
        UserDetails userDetails = new UserDetailsImpl(login.getEmail(), login.getPassWord(), login.getUser().getRole());
        return userDetails;

    }


}
