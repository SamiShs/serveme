package be.fkhu.serveme.account.auth;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * The type Security adapter is a spring WebSecurityConfigurerAdapter.
 *
 * @author Samirullah
 */
@EnableWebSecurity
public class SecurityAdapter extends WebSecurityConfigurerAdapter {

    //
    private final UserDetailsService userDetailsService;


    //the 'AuthenticationManager' Object is used in 'LoginService' to get an authentication object from by passing the 'LoginRequest'
    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /**
     * Instantiates a new Security adapter.
     *
     * @param userDetailsService :a service class of which the 'loadUserByUsername' returns a UserDetails Object by which Spring performs the login authentication
     */
    @Autowired
    public SecurityAdapter(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

// This overridden configure method is used to secure specific directories.
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable().addFilterAfter(new LoginPageFilter(), UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers("/user").hasAnyRole("CUSTOMER", "MANAGER")
                .and()
                .formLogin()
                .loginProcessingUrl("/login")
                .loginPage("/login")
//                .successHandler(successHandler)
                .and()
                .logout().logoutSuccessUrl("/")
        ;
//                .antMatchers("/manager/**").hasRole("MANAGER")
//                .antMatchers("/customer/**").hasRole("CUSTOMER")
//                .antMatchers("/auth/**","/home","/logout","/**","/css/**","/static/**")
//                .permitAll()
//                .anyRequest()
//                .authenticated()
//                .and().logout()
//                .logoutUrl("/auth/logout")
//                .logoutSuccessUrl("/index");

    }

    /**
     * This is a bean for Password encoder
     * BCryptPasswordEncoder from Spring is most used for this purpose
     *
     * @return the password encoder
     */
// this bean returns a PasswordEncoder for string Encoding. .
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}


