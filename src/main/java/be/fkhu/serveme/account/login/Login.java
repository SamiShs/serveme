package be.fkhu.serveme.account.login;

import be.fkhu.serveme.account.user.User;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Samirullah & Mauro
 */

@Entity
@Table(name = "Login")
public class Login implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Login_ID")

    private long Login_ID;
    @Column(name = "email")
    private String email;
    @Column(name = "password")
    private String passWord;
    @OneToOne(mappedBy = "login")
    private User user;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Login login = (Login) o;
        return email.equals(login.email) &&
                passWord.equals(login.passWord);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, passWord);
    }


}
