package be.fkhu.serveme.account.login;


import be.fkhu.serveme.account.login.Login;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 *
 * @author Samirullah & Mauro
 */
@Repository
public interface LoginRepository extends JpaRepository<Login, Long> {

    Optional<Login> findLoginByEmail(String email);

        //Optional<Login> findLoginById(long id);

        //void createAccount(Account account);

//        void updateAccount(Account account);
//
//        void deleteAccount(long id);
//
//        void deleteAccount(Account account);

}
