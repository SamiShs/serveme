open module be.fkhu.serveme {

    //beans
    requires spring.beans;
    requires java.annotation;
    requires java.validation;

    //spring
    requires spring.core;
    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires spring.context;

    //authentication
    requires spring.security.config;
    requires spring.security.core;
    requires spring.security.web;

    //database
    requires java.sql;
    requires java.persistence;
    requires spring.jdbc;
    requires spring.boot.starter.jdbc;
    requires spring.boot.starter.data.jpa;
    requires spring.data.jpa;
    requires spring.data.commons;

    //server
    requires spring.web;
    requires tomcat.embed.core;
    requires okhttp;

    //    Binding
    requires java.xml.bind;
    requires com.sun.xml.bind;
    //requires jackson.annotations;
    //  Not Modularized - NEED THIS !
    requires net.bytebuddy;
    requires spring.webmvc;
    requires spring.tx;
    requires java.json;
}